from ROOT import *
from Channel import *

class Event:
    def __init__(self,Eventnumber): 
      self.Eventnumber = Eventnumber
      self.ReferenceChannelList = []
      self.ReflectionChannelList = []
      self.ReferenceIsOK = False
      self.IsReferenceHit = False
      self.ReferenceHitChannel = -1
      self.ReferenceHitPeakValue = 0.
      self.ReferenceHitToT = 0.
      self.ReferenceHitLETime = 0

      self.IsReflectionHit = False
      self.ReflectionHitChannel = -1
      self.IsHit = False
      self.HitOrignalPeakValue = 0.
      self.HitOrignalPeakBin   = 0 
      self.IsMuonBundle = False
      self.IsReflectionOK = False
      self.ReflectionOnlyTwoPeak = True
      self.ReflectionPeakNum = 0
      
      tempChannelPeak = 0.
      referSigNum = 0
      for idindex in range(ReferenceStartChannel,ReferenceEndChannel+1):
          referChannel = Channel(self.Eventnumber,idindex)
          referChannel.ReferenceUnpackData()
          referChannel.ReferenceRmsCalculate()
          referChannel.ReferenceFindPeak()
          self.ReferenceChannelList.append(referChannel)
      if self.ReferenceChannelList[15].ReferenceIsSignal == False:
          for idindex in range(15):
              self.ReferenceChannelList[idindex].ReferencePeakValue = self.ReferenceChannelList[idindex].ReferencePeakValue - self.ReferenceChannelList[15].ReferencePeakValue    
              if self.ReferenceChannelList[idindex].ReferencePeakValue < 5:
                  self.ReferenceChannelList[idindex].ReferenceIsSignal = False    
      for idindex in range(ReferenceStartChannel,ReferenceEndChannel+1):    
          if self.ReferenceChannelList[idindex].ReferenceIsSignal == True :
              referSigNum += 1 
              self.ReferenceChannelList[idindex].ReferenceToTCal()
              self.ReferenceIsOK = True
              tempChannelPeak = self.ReferenceChannelList[idindex].ReferencePeakValue
              if tempChannelPeak > self.ReferenceHitPeakValue:
                  self.ReferenceHitPeakValue = tempChannelPeak
                  self.ReferenceHitChannel = idindex
                  self.ReferenceHitToT = self.ReferenceChannelList[idindex].ReferenceToT
      self.ReferenceHitLETime = self.ReferenceChannelList[self.ReferenceHitChannel].ReferenceLETime
      if self.ReferenceIsOK == True and 110.9 < self.ReferenceHitLETime < 130.1 and self.ReferenceHitToT < 1 and referSigNum < 16:
          self.IsReferenceHit = True
   
      for idindex in range(ReflectionStartChannel,ReflectionEndChannel+1):
          reflectionChannel = Channel(self.Eventnumber,idindex)
          reflectionChannel.ReflectionUnpackData()
          reflectionChannel.ReflectionFFT()
          reflectionChannel.ReflectionAfterFFTRmsCalculate()
          reflectionChannel.FindOriginalSignalPeak()
          reflectionChannel.OriginalSignalParaDetermination()
          reflectionChannel.FindReflectedSignalPeak()
          reflectionChannel.ReflectedSignalParaDetermination()
          reflectionChannel.ReflectionTwoLEFineCal()
          reflectionChannel.Timediff()
          reflectionChannel.IsReflectedChannelHit()
          self.ReflectionChannelList.append(reflectionChannel)
          if self.HitOrignalPeakValue > reflectionChannel.OrignalPeakValue:
              self.HitOrignalPeakValue = reflectionChannel.OrignalPeakValue
              self.ReflectionHitChannel = idindex - 15
              self.HitOrignalPeakBin = reflectionChannel.OrignalPeakBin
      if self.ReflectionChannelList[self.ReflectionHitChannel - 1].ReflectionIsSignal == True:
          self.IsReflectionHit = True
      if self.IsReflectionHit == True and self.IsReferenceHit == True:
          self.IsHit = True

    def MuonBundleDetermination(self):
        if self.ReferenceHitChannel > 1:
            for leftindex in range(1,self.ReferenceHitChannel):
                if self.ReferenceChannelList[self.ReferenceHitChannel - leftindex - 1].ReferencePeakValue > self.ReferenceChannelList[self.ReferenceHitChannel - leftindex].ReferencePeakValue + 5: 
                    self.IsMuonBundle = True
                    break
        if self.ReferenceHitChannel < 14:
            for rightindex in range(1,15 - self.ReferenceHitChannel):
                if self.ReferenceChannelList[self.ReferenceHitChannel + rightindex + 1].ReferencePeakValue > self.ReferenceChannelList[self.ReferenceHitChannel + rightindex].ReferencePeakValue + 5: 
                    self.IsMuonBundle = True
                    break 
        if self.ReflectionHitChannel > 2:
            for leftindex in range(1,self.ReflectionHitChannel - 1):
                if self.ReflectionChannelList[self.ReflectionHitChannel - leftindex - 2].OrignalPeakValue  < self.ReflectionChannelList[self.ReflectionHitChannel - leftindex - 1].OrignalPeakValue - 3: 
                    self.IsMuonBundle = True
                    break
        if self.ReflectionHitChannel < 5:
            for rightindex in range(1,6 - self.ReflectionHitChannel):
                if self.ReflectionChannelList[self.ReflectionHitChannel + rightindex].OrignalPeakValue < self.ReflectionChannelList[self.ReflectionHitChannel + rightindex - 1].OrignalPeakValue - 3: 
                    self.IsMuonBundle = True
                    break
        

    def ReflectionFindMorePeak(self):
        self.ReflectionPeakNum = 0
        Peak2Value = 0
        Peak2Bin = 0
        Peak1Bin = 0
        if self.IsReflectionHit == True:
            Peak1Value = self.HitOrignalPeakValue
            ChosenChannel = self.ReflectionChannelList[self.ReflectionHitChannel - 1]
            if Peak1Value < - 3:
                self.ReflectionPeakNum = 1
                Peak1Bin = self.HitOrignalPeakBin
                for index in range(Peak1Bin-5,ReflectionSignalStartBin,-1):
                    if ChosenChannel.CorVoltList[index] < -2 and ChosenChannel.CorVoltList[index-2]> ChosenChannel.CorVoltList[index-1]>ChosenChannel.CorVoltList[index] and ChosenChannel.CorVoltList[index+2]> ChosenChannel.CorVoltList[index+1]> ChosenChannel.CorVoltList[index] and (ChosenChannel.CorVoltList[index]/Peak1Value) > 0.4:
                        self.ReflectionPeakNum += 1
                        break
                for index in range(Peak1Bin+5,ReflectionSignalEndBin):
                    if ChosenChannel.CorVoltList[index] < -2 and ChosenChannel.CorVoltList[index-2]> ChosenChannel.CorVoltList[index-1]>ChosenChannel.CorVoltList[index] and ChosenChannel.CorVoltList[index+2]> ChosenChannel.CorVoltList[index+1]> ChosenChannel.CorVoltList[index] and (ChosenChannel.CorVoltList[index]/Peak1Value) > 0.4:
                        Peak2Bin = index
                        Peak2Value = ChosenChannel.CorVoltList[index]
                        self.ReflectionPeakNum += 1
                        break
                for index in range(Peak2Bin+5,ReflectionSignalEndBin):
                    if ChosenChannel.CorVoltList[index] < -2 and ChosenChannel.CorVoltList[index-2]> ChosenChannel.CorVoltList[index-1]>ChosenChannel.CorVoltList[index] and ChosenChannel.CorVoltList[index+2]> ChosenChannel.CorVoltList[index+1]> ChosenChannel.CorVoltList[index] and (ChosenChannel.CorVoltList[index]/Peak2Value) > 0.4:
                        self.ReflectionPeakNum += 1
                        break   
            if 0 > Peak1Value > -3:
                for index in range(ReflectionSignalStartBin,ReflectionSignalEndBin):
                    if ChosenChannel.CorVoltList[index] < - 5*ChosenChannel.ReflectionRms and ChosenChannel.CorVoltList[index-2]> ChosenChannel.CorVoltList[index-1]>ChosenChannel.CorVoltList[index] and ChosenChannel.CorVoltList[index+2]> ChosenChannel.CorVoltList[index+1]> ChosenChannel.CorVoltList[index]:
                        self.ReflectionPeakNum += 1
                        Peak1Bin = index
                        break
                for index in range(Peak1Bin+5,ReflectionSignalEndBin):
                    if ChosenChannel.CorVoltList[index] < - 5*ChosenChannel.ReflectionRms and ChosenChannel.CorVoltList[index-2]> ChosenChannel.CorVoltList[index-1]>ChosenChannel.CorVoltList[index] and ChosenChannel.CorVoltList[index+2]> ChosenChannel.CorVoltList[index+1]> ChosenChannel.CorVoltList[index]:
                        self.ReflectionPeakNum += 1
                        Peak2Bin = index
                        break
                for index in range(Peak2Bin+5,ReflectionSignalEndBin):
                    if ChosenChannel.CorVoltList[index] < - 5*ChosenChannel.ReflectionRms and ChosenChannel.CorVoltList[index-2]> ChosenChannel.CorVoltList[index-1]>ChosenChannel.CorVoltList[index] and ChosenChannel.CorVoltList[index+2]> ChosenChannel.CorVoltList[index+1]> ChosenChannel.CorVoltList[index]:
                        self.ReflectionPeakNum += 1
                        break
        if self.ReflectionPeakNum > 2:
            self.ReflectionOnlyTwoPeak = False


          

    

        