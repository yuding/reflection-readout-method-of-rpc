ReferenceSignalStartBin = 450
ReferenceSignalEndBin = 800
ReferenceStartChannel = 0
ReferenceEndChannel = 15
ReferenceBinNum = 1024    # one bin: 0.2ns
Time_Bin_rate = 0.2       # converted into [ns]
Volt_Bin_rate = 1000.0 / 4096    #  converted into [mV]
floatbyte = 4


# time: one bin -- 0.1ns
ReflectionSignalStartBin = 930   
ReflectionSignalEndBin = 1430
ReflectionStartChannel = 16
ReflectionEndChannel = 21

ReferenceInputPath = "/Users/dingyuexin/Reflection Readout/Data/SADC_DATA/0810-1-676"
ReferenceInputPath2 = "/Users/dingyuexin/Reflection Readout/Data/SADC_DATA/0810-2-515"
ReferenceInputPath3 = "/Users/dingyuexin/Reflection Readout/Data/SADC_DATA/0810-3-614"
ReferenceInputPath4 = "/Users/dingyuexin/Reflection Readout/Data/SADC_DATA/0810-4-587"
ReferenceInputPath5 = "/Users/dingyuexin/Reflection Readout/Data/SADC_DATA/0810-5-501"
ReferenceInputPath6 = "/Users/dingyuexin/Reflection Readout/Data/SADC_DATA/0811-1-400"
ReferenceInputPath7 = "/Users/dingyuexin/Reflection Readout/Data/SADC_DATA/0811-2-502"
ReferenceInputPath8 = "/Users/dingyuexin/Reflection Readout/Data/SADC_DATA/0727-5-501"
ReferenceInputPath9 = "/Users/dingyuexin/Reflection Readout/Data/SADC_DATA/0727-5-507"
ReferenceInputPath10 = "/Users/dingyuexin/Reflection Readout/Data/SADC_DATA/0811-3-617"
ReferenceInputPath11 = "/Users/dingyuexin/Reflection Readout/Data/SADC_DATA/0811-4-535"
ReferenceInputPath12 = "/Users/dingyuexin/Reflection Readout/Data/SADC_DATA/0815-1-530"
ReferenceInputPath13 = "/Users/dingyuexin/Reflection Readout/Data/SADC_DATA/0815-2-490"
ReferenceInputPath14 = "/Users/dingyuexin/Reflection Readout/Data/SADC_DATA/0815-3-519"
ReferenceInputPath15 = "/Users/dingyuexin/Reflection Readout/Data/SADC_DATA/0815-4-500"
ReferenceInputPath16 = "/Users/dingyuexin/Reflection Readout/Data/SADC_DATA/0816-1-628"
ReferenceInputPath17 = "/Users/dingyuexin/Reflection Readout/Data/SADC_DATA/0816-2-574"
ReferenceInputPath18 = "/Users/dingyuexin/Reflection Readout/Data/SADC_DATA/0816-3-591"
ReferenceInputPath19 = "/Users/dingyuexin/Reflection Readout/Data/SADC_DATA/0816-4-511"

ReferenceDataInputList = []
ReferenceDataInputList2 = []
ReferenceDataInputList3 = []
ReferenceDataInputList4 = []
ReferenceDataInputList5 = []
ReferenceDataInputList6 = []
ReferenceDataInputList7 = []
ReferenceDataInputList8 = []
ReferenceDataInputList9 = []
ReferenceDataInputList10 = []
ReferenceDataInputList11 = []
ReferenceDataInputList12 = []
ReferenceDataInputList13 = []
ReferenceDataInputList14 = []
ReferenceDataInputList15 = []
ReferenceDataInputList16 = []
ReferenceDataInputList17 = []
ReferenceDataInputList18 = []
ReferenceDataInputList19 = []

for id in range(16):
    with open(ReferenceInputPath+'/wave_'+str(id)+'.dat', 'rb') as inFile:
                    dataReference = inFile.read()
                    ReferenceDataInputList.append(dataReference)          # read .dat file
    with open(ReferenceInputPath2+'/wave_'+str(id)+'.dat', 'rb') as inFile:
                    dataReference = inFile.read()
                    ReferenceDataInputList2.append(dataReference)
    with open(ReferenceInputPath3+'/wave_'+str(id)+'.dat', 'rb') as inFile:
                    dataReference = inFile.read()
                    ReferenceDataInputList3.append(dataReference)
    with open(ReferenceInputPath4+'/wave_'+str(id)+'.dat', 'rb') as inFile:
                    dataReference = inFile.read()
                    ReferenceDataInputList4.append(dataReference)
    with open(ReferenceInputPath5+'/wave_'+str(id)+'.dat', 'rb') as inFile:
                    dataReference = inFile.read()
                    ReferenceDataInputList5.append(dataReference)
    with open(ReferenceInputPath6+'/wave_'+str(id)+'.dat', 'rb') as inFile:
                    dataReference = inFile.read()
                    ReferenceDataInputList6.append(dataReference)
    with open(ReferenceInputPath7+'/wave_'+str(id)+'.dat', 'rb') as inFile:
                    dataReference = inFile.read()
                    ReferenceDataInputList7.append(dataReference)
    with open(ReferenceInputPath8+'/wave_'+str(id)+'.dat', 'rb') as inFile:
                    dataReference = inFile.read()
                    ReferenceDataInputList8.append(dataReference)
    with open(ReferenceInputPath9+'/wave_'+str(id)+'.dat', 'rb') as inFile:
                    dataReference = inFile.read()
                    ReferenceDataInputList9.append(dataReference)
    with open(ReferenceInputPath10+'/wave_'+str(id)+'.dat', 'rb') as inFile:
                    dataReference = inFile.read()
                    ReferenceDataInputList10.append(dataReference)
    with open(ReferenceInputPath11+'/wave_'+str(id)+'.dat', 'rb') as inFile:
                    dataReference = inFile.read()
                    ReferenceDataInputList11.append(dataReference)
    with open(ReferenceInputPath12+'/wave_'+str(id)+'.dat', 'rb') as inFile:
                    dataReference = inFile.read()
                    ReferenceDataInputList12.append(dataReference)
    with open(ReferenceInputPath13+'/wave_'+str(id)+'.dat', 'rb') as inFile:
                    dataReference = inFile.read()
                    ReferenceDataInputList13.append(dataReference)
    with open(ReferenceInputPath14+'/wave_'+str(id)+'.dat', 'rb') as inFile:
                    dataReference = inFile.read()
                    ReferenceDataInputList14.append(dataReference)
    with open(ReferenceInputPath15+'/wave_'+str(id)+'.dat', 'rb') as inFile:
                    dataReference = inFile.read()
                    ReferenceDataInputList15.append(dataReference)
    with open(ReferenceInputPath16+'/wave_'+str(id)+'.dat', 'rb') as inFile:
                    dataReference = inFile.read()
                    ReferenceDataInputList16.append(dataReference)
    with open(ReferenceInputPath17+'/wave_'+str(id)+'.dat', 'rb') as inFile:
                    dataReference = inFile.read()
                    ReferenceDataInputList17.append(dataReference)
    with open(ReferenceInputPath18+'/wave_'+str(id)+'.dat', 'rb') as inFile:
                    dataReference = inFile.read()
                    ReferenceDataInputList18.append(dataReference)
    with open(ReferenceInputPath19+'/wave_'+str(id)+'.dat', 'rb') as inFile:
                    dataReference = inFile.read()
                    ReferenceDataInputList19.append(dataReference)

eventlen = int(len(ReferenceDataInputList[0])/(floatbyte * ReferenceBinNum))
eventlen2 = int(len(ReferenceDataInputList2[0])/(floatbyte * ReferenceBinNum))
eventlen3 = int(len(ReferenceDataInputList3[0])/(floatbyte * ReferenceBinNum))
eventlen4 = int(len(ReferenceDataInputList4[0])/(floatbyte * ReferenceBinNum))
eventlen5 = int(len(ReferenceDataInputList5[0])/(floatbyte * ReferenceBinNum)) 
eventlen6 = int(len(ReferenceDataInputList6[0])/(floatbyte * ReferenceBinNum)) 
eventlen7 = int(len(ReferenceDataInputList7[0])/(floatbyte * ReferenceBinNum)) 
eventlen8 = int(len(ReferenceDataInputList8[0])/(floatbyte * ReferenceBinNum)) 
eventlen9 = int(len(ReferenceDataInputList9[0])/(floatbyte * ReferenceBinNum)) 
eventlen10 = int(len(ReferenceDataInputList10[0])/(floatbyte * ReferenceBinNum)) 
eventlen11 = int(len(ReferenceDataInputList11[0])/(floatbyte * ReferenceBinNum)) 
eventlen12 = int(len(ReferenceDataInputList12[0])/(floatbyte * ReferenceBinNum)) 
eventlen13 = int(len(ReferenceDataInputList13[0])/(floatbyte * ReferenceBinNum)) 
eventlen14 = int(len(ReferenceDataInputList14[0])/(floatbyte * ReferenceBinNum)) 
eventlen15 = int(len(ReferenceDataInputList15[0])/(floatbyte * ReferenceBinNum)) 
eventlen16 = int(len(ReferenceDataInputList16[0])/(floatbyte * ReferenceBinNum)) 
eventlen17 = int(len(ReferenceDataInputList17[0])/(floatbyte * ReferenceBinNum)) 
eventlen18 = int(len(ReferenceDataInputList18[0])/(floatbyte * ReferenceBinNum)) 
eventlen19 = int(len(ReferenceDataInputList19[0])/(floatbyte * ReferenceBinNum)) 

ReflectionInputPath = "/Users/dingyuexin/Reflection Readout/Data/Oscilloscope Data/0810-1/"
ReflectionInputPath2 = "/Users/dingyuexin/Reflection Readout/Data/Oscilloscope Data/0810-2/"
ReflectionInputPath3 = "/Users/dingyuexin/Reflection Readout/Data/Oscilloscope Data/0810-3/"
ReflectionInputPath4 = "/Users/dingyuexin/Reflection Readout/Data/Oscilloscope Data/0811-1/"
ReflectionInputPath5 = "/Users/dingyuexin/Reflection Readout/Data/Oscilloscope Data/0727-3/"
ReflectionInputPath6 = "/Users/dingyuexin/Reflection Readout/Data/Oscilloscope Data/0811-2/"
ReflectionInputPath7 = "/Users/dingyuexin/Reflection Readout/Data/Oscilloscope Data/0815-1/"
ReflectionInputPath8 = "/Users/dingyuexin/Reflection Readout/Data/Oscilloscope Data/0815-2/"
ReflectionInputPath9 = "/Users/dingyuexin/Reflection Readout/Data/Oscilloscope Data/0816-1/"
ReflectionInputPath10 = "/Users/dingyuexin/Reflection Readout/Data/Oscilloscope Data/0816-2/"