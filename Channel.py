from ROOT import *
from ParaAndDataInput import *
import struct
import math
from array import *
import lecroyparser
import numpy as np


class Channel:
    def __init__(self, eventNumber, channelId):
        self.EventNumber              = eventNumber
        self.ChannelId                = channelId
        self.TimeList                 = []
        self.PreVoltList              = []
        self.CorVoltList              = []
        self.ReferenceBaselineValue   = 0.
        self.ReferenceRms             = 0.
        self.ReferenceLETime          = 0.
        self.ReferenceLEBin           = 0
        self.ReferenceFETime          = 0.
        self.ReferenceFEBin           = 0
        self.ReferenceToT             = 0.
        self.ReferencePeakTime        = 0.
        self.ReferencePeakValue       = 0.
        self.ReferencePeakBin         = 0
        self.ReferenceLEThreshold     = 0.5  # 0.5 * PeakValue (Reference)
        self.ReferenceIsSignal        = False
        self.ReferencePeakThresholdOfRMS   =  5   # 5 * RMS
        self.ReferencePeakThresholdOfValue  =  5   # 5mV
        
        self.ReflectionBinSize        = 0
        self.ReflectionBaselineValue  = 0.
        self.ReflectionRms            = 0.
        self.OrignalPeakValue         = 0.
        self.ReflectedPeakValue       = 0.
        self.OrignalPeakBin           = 0
        self.Signal1LEindex           = 0
        self.Signal1FEindex           = 0
        self.Signal1Endindex          = 0
        self.Signal2LEindex           = 0
        self.Signal2FEindex           = 0
        self.ReflectedPeakBin         = 0
        self.Signal1LEThreshold       = 0  
        self.Signal2LEThreshold       = 0
        self.ReflectionLEThreshold    = 0.7  # 0.7 * PeakValue (Reference)
        self.Signal1LETime50          = 0
        self.Signal1LETime60          = 0
        self.Signal1LETime80          = 0
        self.Signal1LETime90          = 0
        self.Signal1LE50Bin           = 0
        self.Signal1LE60Bin           = 0
        self.Signal1LE80Bin           = 0
        self.Signal1LE90Bin           = 0
        self.Signal2LETime50          = 0
        self.Signal2LETime60          = 0
        self.Signal2LETime80          = 0
        self.Signal2LETime90          = 0
        self.Signal2LE50Bin           = 0
        self.Signal2LE60Bin           = 0
        self.Signal2LE80Bin           = 0
        self.Signal2LE90Bin           = 0
        self.TimeGap                  = 0
        self.TimeGap50                = 0
        self.TimeGap60                = 0
        self.TimeGap70                = 0
        self.TimeGap80                = 0
        self.TimeGap90                = 0
        self.ReflectionIsSignal       = False
        self.ReflectionTimeArr        = array('f',[-1.,-1.,-1.,-1.,-1.,-1.])
        self.AfterFFTAmpArr            = 0
        self.IsFFT                     = False


    def ReferenceUnpackData(self):
        if self.EventNumber < eventlen:
            for i in range(ReferenceBinNum):
                StartPos = self.EventNumber * floatbyte * ReferenceBinNum + floatbyte * i
                StopPos = StartPos + floatbyte
                bytes_str = ReferenceDataInputList[self.ChannelId][StartPos : StopPos]
                xValue = i * Time_Bin_rate
                self.TimeList.append(xValue)
                yValue = struct.unpack('f', bytes_str)[0] * Volt_Bin_rate
                self.PreVoltList.append(yValue)
        if  eventlen - 1 < self.EventNumber < eventlen + eventlen2:
            for i in range(ReferenceBinNum):
                StartPos = (self.EventNumber-eventlen) * floatbyte * ReferenceBinNum + floatbyte * i
                StopPos = StartPos + floatbyte
                bytes_str = ReferenceDataInputList2[self.ChannelId][StartPos : StopPos]
                xValue = i * Time_Bin_rate
                self.TimeList.append(xValue)
                yValue = struct.unpack('f', bytes_str)[0] * Volt_Bin_rate
                self.PreVoltList.append(yValue)
        if  eventlen + eventlen2 - 1 < self.EventNumber < eventlen + eventlen2 + eventlen3:
           for i in range(ReferenceBinNum):
                StartPos = (self.EventNumber-eventlen-eventlen2) * floatbyte * ReferenceBinNum + floatbyte * i
                StopPos = StartPos + floatbyte
                bytes_str = ReferenceDataInputList3[self.ChannelId][StartPos : StopPos]
                xValue = i * Time_Bin_rate
                self.TimeList.append(xValue)
                yValue = struct.unpack('f', bytes_str)[0] * Volt_Bin_rate
                self.PreVoltList.append(yValue)
        if  eventlen + eventlen2 + eventlen3 - 1 < self.EventNumber < eventlen + eventlen2 + eventlen3 + eventlen4:
            for i in range(ReferenceBinNum):
                StartPos = (self.EventNumber-eventlen-eventlen2-eventlen3) * floatbyte * ReferenceBinNum + floatbyte * i
                StopPos = StartPos + floatbyte
                bytes_str = ReferenceDataInputList4[self.ChannelId][StartPos : StopPos]
                xValue = i * Time_Bin_rate
                self.TimeList.append(xValue)
                yValue = struct.unpack('f', bytes_str)[0] * Volt_Bin_rate
                self.PreVoltList.append(yValue)
        if  eventlen + eventlen2 + eventlen3 + eventlen4 - 1 < self.EventNumber < eventlen + eventlen2 + eventlen3 + eventlen4 + eventlen5:
            for i in range(ReferenceBinNum):
                StartPos = (self.EventNumber-eventlen-eventlen2-eventlen3 - eventlen4) * floatbyte * ReferenceBinNum + floatbyte * i
                StopPos = StartPos + floatbyte
                bytes_str = ReferenceDataInputList5[self.ChannelId][StartPos : StopPos]
                xValue = i * Time_Bin_rate
                self.TimeList.append(xValue)
                yValue = struct.unpack('f', bytes_str)[0] * Volt_Bin_rate
                self.PreVoltList.append(yValue)
        if  eventlen + eventlen2 + eventlen3 + eventlen4 + eventlen5 - 1 < self.EventNumber < eventlen + eventlen2 + eventlen3 + eventlen4 + eventlen5 + eventlen6 :
            for i in range(ReferenceBinNum):
                StartPos = (self.EventNumber-eventlen-eventlen2-eventlen3 - eventlen4 - eventlen5) * floatbyte * ReferenceBinNum + floatbyte * i
                StopPos = StartPos + floatbyte
                bytes_str = ReferenceDataInputList6[self.ChannelId][StartPos : StopPos]
                xValue = i * Time_Bin_rate
                self.TimeList.append(xValue)
                yValue = struct.unpack('f', bytes_str)[0] * Volt_Bin_rate
                self.PreVoltList.append(yValue)
        if  eventlen + eventlen2 + eventlen3 + eventlen4 + eventlen5 + eventlen6 - 1 < self.EventNumber < eventlen + eventlen2 + eventlen3 + eventlen4 + eventlen5 + eventlen6 + eventlen7 :
            for i in range(ReferenceBinNum):
                StartPos = (self.EventNumber-eventlen-eventlen2-eventlen3 - eventlen4 - eventlen5 - eventlen6) * floatbyte * ReferenceBinNum + floatbyte * i
                StopPos = StartPos + floatbyte
                bytes_str = ReferenceDataInputList7[self.ChannelId][StartPos : StopPos]
                xValue = i * Time_Bin_rate
                self.TimeList.append(xValue)
                yValue = struct.unpack('f', bytes_str)[0] * Volt_Bin_rate
                self.PreVoltList.append(yValue)
        if  eventlen + eventlen2 + eventlen3 + eventlen4 + eventlen5 + eventlen6 + eventlen7 - 1 < self.EventNumber < eventlen + eventlen2 + eventlen3 + eventlen4 + eventlen5 + eventlen6 + eventlen7 + eventlen8:
            for i in range(ReferenceBinNum):
                StartPos = (self.EventNumber-eventlen-eventlen2-eventlen3 - eventlen4 - eventlen5 - eventlen6 - eventlen7) * floatbyte * ReferenceBinNum + floatbyte * i
                StopPos = StartPos + floatbyte
                bytes_str = ReferenceDataInputList8[self.ChannelId][StartPos : StopPos]
                xValue = i * Time_Bin_rate
                self.TimeList.append(xValue)
                yValue = struct.unpack('f', bytes_str)[0] * Volt_Bin_rate
                self.PreVoltList.append(yValue)
        if  eventlen + eventlen2 + eventlen3 + eventlen4 + eventlen5 + eventlen6 + eventlen7 + eventlen8 - 1 < self.EventNumber < eventlen + eventlen2 + eventlen3 + eventlen4 + eventlen5 + eventlen6 + eventlen7 + eventlen8 + eventlen9:
            for i in range(ReferenceBinNum):
                StartPos = (self.EventNumber-eventlen-eventlen2-eventlen3 - eventlen4 - eventlen5 - eventlen6 - eventlen7 - eventlen8) * floatbyte * ReferenceBinNum + floatbyte * i
                StopPos = StartPos + floatbyte
                bytes_str = ReferenceDataInputList9[self.ChannelId][StartPos : StopPos]
                xValue = i * Time_Bin_rate
                self.TimeList.append(xValue)
                yValue = struct.unpack('f', bytes_str)[0] * Volt_Bin_rate
                self.PreVoltList.append(yValue)
        if  eventlen + eventlen2 + eventlen3 + eventlen4 + eventlen5 + eventlen6 + eventlen7 + eventlen8 + eventlen9 - 1 < self.EventNumber < eventlen + eventlen2 + eventlen3 + eventlen4 + eventlen5 + eventlen6 + eventlen7 + eventlen8 + eventlen9 + eventlen10 :
            for i in range(ReferenceBinNum):
                StartPos = (self.EventNumber-eventlen-eventlen2-eventlen3 - eventlen4 - eventlen5 - eventlen6 - eventlen7 - eventlen8 - eventlen9) * floatbyte * ReferenceBinNum + floatbyte * i
                StopPos = StartPos + floatbyte
                bytes_str = ReferenceDataInputList10[self.ChannelId][StartPos : StopPos]
                xValue = i * Time_Bin_rate
                self.TimeList.append(xValue)
                yValue = struct.unpack('f', bytes_str)[0] * Volt_Bin_rate
                self.PreVoltList.append(yValue)
        if  eventlen + eventlen2 + eventlen3 + eventlen4 + eventlen5 + eventlen6 + eventlen7 + eventlen8 + eventlen9 + eventlen10 - 1 < self.EventNumber < eventlen + eventlen2 + eventlen3 + eventlen4 + eventlen5 + eventlen6 + eventlen7 + eventlen8 + eventlen9 + eventlen10 + eventlen11 :
            for i in range(ReferenceBinNum):
                StartPos = (self.EventNumber-eventlen-eventlen2-eventlen3 - eventlen4 - eventlen5 - eventlen6 - eventlen7 - eventlen8 - eventlen9 - eventlen10) * floatbyte * ReferenceBinNum + floatbyte * i
                StopPos = StartPos + floatbyte
                bytes_str = ReferenceDataInputList11[self.ChannelId][StartPos : StopPos]
                xValue = i * Time_Bin_rate
                self.TimeList.append(xValue)
                yValue = struct.unpack('f', bytes_str)[0] * Volt_Bin_rate
                self.PreVoltList.append(yValue)
        if  eventlen + eventlen2 + eventlen3 + eventlen4 + eventlen5 + eventlen6 + eventlen7 + eventlen8 + eventlen9 + eventlen10 + eventlen11 - 1 < self.EventNumber < eventlen + eventlen2 + eventlen3 + eventlen4 + eventlen5 + eventlen6 + eventlen7 + eventlen8 + eventlen9 + eventlen10 + eventlen11 + eventlen12:
            for i in range(ReferenceBinNum):
                StartPos = (self.EventNumber-eventlen-eventlen2-eventlen3 - eventlen4 - eventlen5 - eventlen6 - eventlen7 - eventlen8 - eventlen9 - eventlen10 - eventlen11) * floatbyte * ReferenceBinNum + floatbyte * i
                StopPos = StartPos + floatbyte
                bytes_str = ReferenceDataInputList12[self.ChannelId][StartPos : StopPos]
                xValue = i * Time_Bin_rate
                self.TimeList.append(xValue)
                yValue = struct.unpack('f', bytes_str)[0] * Volt_Bin_rate
                self.PreVoltList.append(yValue)
        if  eventlen + eventlen2 + eventlen3 + eventlen4 + eventlen5 + eventlen6 + eventlen7 + eventlen8 + eventlen9 + eventlen10 + eventlen11 + eventlen12 - 1 < self.EventNumber < eventlen + eventlen2 + eventlen3 + eventlen4 + eventlen5 + eventlen6 + eventlen7 + eventlen8 + eventlen9 + eventlen10 + eventlen11 + eventlen12 + eventlen13:
            for i in range(ReferenceBinNum):
                StartPos = (self.EventNumber-eventlen-eventlen2-eventlen3 - eventlen4 - eventlen5 - eventlen6 - eventlen7 - eventlen8 - eventlen9 - eventlen10 - eventlen11 - eventlen12) * floatbyte * ReferenceBinNum + floatbyte * i
                StopPos = StartPos + floatbyte
                bytes_str = ReferenceDataInputList13[self.ChannelId][StartPos : StopPos]
                xValue = i * Time_Bin_rate
                self.TimeList.append(xValue)
                yValue = struct.unpack('f', bytes_str)[0] * Volt_Bin_rate
                self.PreVoltList.append(yValue)
        if  eventlen + eventlen2 + eventlen3 + eventlen4 + eventlen5 + eventlen6 + eventlen7 + eventlen8 + eventlen9 + eventlen10 + eventlen11 + eventlen12 + eventlen13 - 1 < self.EventNumber < eventlen + eventlen2 + eventlen3 + eventlen4 + eventlen5 + eventlen6 + eventlen7 + eventlen8 + eventlen9 + eventlen10 + eventlen11 + eventlen12 + eventlen13 + eventlen14:
            for i in range(ReferenceBinNum):
                StartPos = (self.EventNumber-eventlen-eventlen2-eventlen3 - eventlen4 - eventlen5 - eventlen6 - eventlen7 - eventlen8 - eventlen9 - eventlen10 - eventlen11 - eventlen12 - eventlen13) * floatbyte * ReferenceBinNum + floatbyte * i
                StopPos = StartPos + floatbyte
                bytes_str = ReferenceDataInputList14[self.ChannelId][StartPos : StopPos]
                xValue = i * Time_Bin_rate
                self.TimeList.append(xValue)
                yValue = struct.unpack('f', bytes_str)[0] * Volt_Bin_rate
                self.PreVoltList.append(yValue)
        if  eventlen + eventlen2 + eventlen3 + eventlen4 + eventlen5 + eventlen6 + eventlen7 + eventlen8 + eventlen9 + eventlen10 + eventlen11 + eventlen12 + eventlen13 + eventlen14 - 1 < self.EventNumber < eventlen + eventlen2 + eventlen3 + eventlen4 + eventlen5 + eventlen6 + eventlen7 + eventlen8 + eventlen9 + eventlen10 + eventlen11 + eventlen12 + eventlen13 + eventlen14 + eventlen15:
            for i in range(ReferenceBinNum):
                StartPos = (self.EventNumber-eventlen-eventlen2-eventlen3 - eventlen4 - eventlen5 - eventlen6 - eventlen7 - eventlen8 - eventlen9 - eventlen10 - eventlen11 - eventlen12 - eventlen13 - eventlen14) * floatbyte * ReferenceBinNum + floatbyte * i
                StopPos = StartPos + floatbyte
                bytes_str = ReferenceDataInputList15[self.ChannelId][StartPos : StopPos]
                xValue = i * Time_Bin_rate
                self.TimeList.append(xValue)
                yValue = struct.unpack('f', bytes_str)[0] * Volt_Bin_rate
                self.PreVoltList.append(yValue)
        if  eventlen + eventlen2 + eventlen3 + eventlen4 + eventlen5 + eventlen6 + eventlen7 + eventlen8 + eventlen9 + eventlen10 + eventlen11 + eventlen12 + eventlen13 + eventlen14 + eventlen15 - 1 < self.EventNumber < eventlen + eventlen2 + eventlen3 + eventlen4 + eventlen5 + eventlen6 + eventlen7 + eventlen8 + eventlen9 + eventlen10 + eventlen11 + eventlen12 + eventlen13 + eventlen14 + eventlen15 + eventlen16:
            for i in range(ReferenceBinNum):
                StartPos = (self.EventNumber-eventlen-eventlen2-eventlen3 - eventlen4 - eventlen5 - eventlen6 - eventlen7 - eventlen8 - eventlen9 - eventlen10 - eventlen11 - eventlen12 - eventlen13 - eventlen14 - eventlen15) * floatbyte * ReferenceBinNum + floatbyte * i
                StopPos = StartPos + floatbyte
                bytes_str = ReferenceDataInputList16[self.ChannelId][StartPos : StopPos]
                xValue = i * Time_Bin_rate
                self.TimeList.append(xValue)
                yValue = struct.unpack('f', bytes_str)[0] * Volt_Bin_rate
                self.PreVoltList.append(yValue)
        if  eventlen + eventlen2 + eventlen3 + eventlen4 + eventlen5 + eventlen6 + eventlen7 + eventlen8 + eventlen9 + eventlen10 + eventlen11 + eventlen12 + eventlen13 + eventlen14 + eventlen15 + eventlen16 - 1 < self.EventNumber < eventlen + eventlen2 + eventlen3 + eventlen4 + eventlen5 + eventlen6 + eventlen7 + eventlen8 + eventlen9 + eventlen10 + eventlen11 + eventlen12 + eventlen13 + eventlen14 + eventlen15 + eventlen16 + eventlen17:
            for i in range(ReferenceBinNum):
                StartPos = (self.EventNumber-eventlen-eventlen2-eventlen3 - eventlen4 - eventlen5 - eventlen6 - eventlen7 - eventlen8 - eventlen9 - eventlen10 - eventlen11 - eventlen12 - eventlen13 - eventlen14 - eventlen15 - eventlen16) * floatbyte * ReferenceBinNum + floatbyte * i
                StopPos = StartPos + floatbyte
                bytes_str = ReferenceDataInputList17[self.ChannelId][StartPos : StopPos]
                xValue = i * Time_Bin_rate
                self.TimeList.append(xValue)
                yValue = struct.unpack('f', bytes_str)[0] * Volt_Bin_rate
                self.PreVoltList.append(yValue)
        if  eventlen + eventlen2 + eventlen3 + eventlen4 + eventlen5 + eventlen6 + eventlen7 + eventlen8 + eventlen9 + eventlen10 + eventlen11 + eventlen12 + eventlen13 + eventlen14 + eventlen15 + eventlen16 + eventlen17 - 1 < self.EventNumber < eventlen + eventlen2 + eventlen3 + eventlen4 + eventlen5 + eventlen6 + eventlen7 + eventlen8 + eventlen9 + eventlen10 + eventlen11 + eventlen12 + eventlen13 + eventlen14 + eventlen15 + eventlen16 + eventlen17 + eventlen18:
            for i in range(ReferenceBinNum):
                StartPos = (self.EventNumber-eventlen-eventlen2-eventlen3 - eventlen4 - eventlen5 - eventlen6 - eventlen7 - eventlen8 - eventlen9 - eventlen10 - eventlen11 - eventlen12 - eventlen13 - eventlen14 - eventlen15 - eventlen16 - eventlen17) * floatbyte * ReferenceBinNum + floatbyte * i
                StopPos = StartPos + floatbyte
                bytes_str = ReferenceDataInputList18[self.ChannelId][StartPos : StopPos]
                xValue = i * Time_Bin_rate
                self.TimeList.append(xValue)
                yValue = struct.unpack('f', bytes_str)[0] * Volt_Bin_rate
                self.PreVoltList.append(yValue)
        if  eventlen + eventlen2 + eventlen3 + eventlen4 + eventlen5 + eventlen6 + eventlen7 + eventlen8 + eventlen9 + eventlen10 + eventlen11 + eventlen12 + eventlen13 + eventlen14 + eventlen15 + eventlen16 + eventlen17 + eventlen18 - 1 < self.EventNumber < eventlen + eventlen2 + eventlen3 + eventlen4 + eventlen5 + eventlen6 + eventlen7 + eventlen8 + eventlen9 + eventlen10 + eventlen11 + eventlen12 + eventlen13 + eventlen14 + eventlen15 + eventlen16 + eventlen17 + eventlen18 + eventlen19:
            for i in range(ReferenceBinNum):
                StartPos = (self.EventNumber-eventlen-eventlen2-eventlen3 - eventlen4 - eventlen5 - eventlen6 - eventlen7 - eventlen8 - eventlen9 - eventlen10 - eventlen11 - eventlen12 - eventlen13 - eventlen14 - eventlen15 - eventlen16 - eventlen17 - eventlen18) * floatbyte * ReferenceBinNum + floatbyte * i
                StopPos = StartPos + floatbyte
                bytes_str = ReferenceDataInputList19[self.ChannelId][StartPos : StopPos]
                xValue = i * Time_Bin_rate
                self.TimeList.append(xValue)
                yValue = struct.unpack('f', bytes_str)[0] * Volt_Bin_rate
                self.PreVoltList.append(yValue)        

    def ReferenceRmsCalculate(self):
        sumvolt = 0
        sumnum = 0
        sumSquare = 0
        for i in range(300):
            sumvolt += self.PreVoltList[i]
            sumnum += 1
        self.ReferenceBaselineValue = sumvolt / sumnum
        for i in range(300):
            sumSquare += math.pow((self.PreVoltList[i]- self.ReferenceBaselineValue), 2)
        self.ReferenceRms = math.sqrt(sumSquare/sumnum)
        for i in range(ReferenceBinNum):
            self.CorVoltList.append(self.PreVoltList[i] - self.ReferenceBaselineValue)
    
    def ReferenceFindPeak(self):
        PeakValue = 0
        for index in range(550,650):
            if self.CorVoltList[index] > PeakValue:
                PeakValue = self.CorVoltList[index]
                if PeakValue > self.ReferencePeakThresholdOfValue and self.CorVoltList[index-2]  < self.CorVoltList[index-1] < PeakValue and self.CorVoltList[index+2] < self.CorVoltList[index+1] < PeakValue :
                    self.ReferenceIsSignal = True
                    self.ReferencePeakValue = PeakValue
                    self.ReferencePeakTime = self.TimeList[index]
                    self.ReferencePeakBin = index

    #def PosCenterCal(self):

    def ReferenceToTCal(self):
        for index in range(self.ReferencePeakBin,0,-1):
            if self.CorVoltList[index] < self.ReferenceLEThreshold * self.ReferencePeakValue and self.CorVoltList[index-1] < self.CorVoltList[index] < self.CorVoltList[index+1]:
                self.ReferenceLEBin = index
                self.ReferenceLETime = self.TimeList[index]
                break
        for index in range(self.ReferencePeakBin,ReferenceSignalEndBin):
            if self.CorVoltList[index] > self.ReferenceLEThreshold * self.ReferencePeakValue and self.CorVoltList[index+1] < self.CorVoltList[index] < self.CorVoltList[index-1]:
                self.ReferenceFEBin = index
                self.ReferenceFETime = self.TimeList[index]
                break
        self.ReferenceToT = self.ReferenceFETime - self.ReferenceLETime

    def ReferenceWaveformDrawing(self):
        c1 = TCanvas("c", "BPRE", 800, 600)
        c1.cd()
        c1.Divide(1,1,0.008,0.007)
        gPad.SetTopMargin(0.09)
        gPad.SetBottomMargin(0.10)
        gPad.SetLeftMargin(0.10)
        gPad.SetRightMargin(0.05)
        TimeArr = array('f',self.TimeList)
        VoltArr = array('f',self.CorVoltList)
        WaveformGraph = TGraph(ReferenceBinNum,TimeArr,VoltArr) 
        WaveformGraph.SetLineColor(1)
        WaveformGraph.SetLineWidth(2)
        WaveformGraph.Draw("AL")
        WaveformGraph.SetTitle("Event "+str(self.EventNumber) + "_Reference_Channel "+str(self.ChannelId)+"_Waveform")
        ax = WaveformGraph.GetXaxis()
        #ax.SetRangeUser(SignalStartBin, SignalEndBin)
        ax.SetTitle( "Time [ns]" )
        ay = WaveformGraph.GetYaxis()
        ay.SetTitle( "Amplitude [mV]" )
        ay.SetRangeUser(self.ReferencePeakValue - 5, 5)
        ay.SetTitleSize(0.04)
        ax.SetTitleOffset(1.0)
        ay.SetTitleOffset(0.8)
        ax.SetTitleSize(0.04)
        ax.SetLabelSize(0.04)
        ay.SetLabelSize(0.04)
        ax.Draw("same")
        ay.Draw("same")
        c1.SaveAs("Event "+str(self.EventNumber) + "_Reference_Channel "+str(self.ChannelId)+"_Waveform.png")

    def ReflectionUnpackData(self):
        if  self.EventNumber < eventlen + eventlen2 :
            InputDataName = 'C%s--Trace--%05d.trc' %(self.ChannelId - 13, self.EventNumber)
            data = lecroyparser.ScopeData(ReflectionInputPath+InputDataName)
            self.TimeList = data.x.tolist()
            self.PreVoltList = data.y.tolist()
        if  eventlen + eventlen2 - 1 < self.EventNumber < eventlen + eventlen2 + eventlen3 + eventlen4:
            InputDataName = 'C%s--Trace--%05d.trc' %(self.ChannelId - 13, self.EventNumber - eventlen - eventlen2)
            data = lecroyparser.ScopeData(ReflectionInputPath2+InputDataName)
            self.TimeList = data.x.tolist()
            self.PreVoltList = data.y.tolist()
        if  eventlen + eventlen2 + eventlen3 + eventlen4 - 1 < self.EventNumber < eventlen + eventlen2 + eventlen3 + eventlen4 + eventlen5:
            InputDataName = 'C%s--Trace--%05d.trc' %(self.ChannelId - 13, self.EventNumber - eventlen - eventlen2 - eventlen3 - eventlen4)
            data = lecroyparser.ScopeData(ReflectionInputPath3+InputDataName)
            self.TimeList = data.x.tolist()
            self.PreVoltList = data.y.tolist()
        if  eventlen + eventlen2 + eventlen3 + eventlen4 + eventlen5 - 1 < self.EventNumber < eventlen + eventlen2 + eventlen3 + eventlen4 + eventlen5 + eventlen6 + eventlen7 :
            InputDataName = 'C%s--Trace--%05d.trc' %(self.ChannelId - 13, self.EventNumber - eventlen - eventlen2 - eventlen3 - eventlen4 - eventlen5)
            data = lecroyparser.ScopeData(ReflectionInputPath4+InputDataName)
            self.TimeList = data.x.tolist()
            self.PreVoltList = data.y.tolist()
        if  eventlen + eventlen2 + eventlen3 + eventlen4 + eventlen5 + eventlen6 + eventlen7 - 1 < self.EventNumber < eventlen + eventlen2 + eventlen3 + eventlen4 + eventlen5 + eventlen6 + eventlen7 + eventlen8 + eventlen9 :
            InputDataName = 'C%s--Trace--%05d.trc' %(self.ChannelId - 13, self.EventNumber - eventlen - eventlen2 - eventlen3 - eventlen4 - eventlen5 - eventlen6 - eventlen7)
            data = lecroyparser.ScopeData(ReflectionInputPath5+InputDataName)
            self.TimeList = data.x.tolist()
            self.PreVoltList = data.y.tolist()
        if  eventlen + eventlen2 + eventlen3 + eventlen4 + eventlen5 + eventlen6 + eventlen7 + eventlen8 + eventlen9 - 1 < self.EventNumber < eventlen + eventlen2 + eventlen3 + eventlen4 + eventlen5 + eventlen6 + eventlen7 + eventlen8 + eventlen9 + eventlen10 + eventlen11:
            InputDataName = 'C%s--Trace--%05d.trc' %(self.ChannelId - 13, self.EventNumber - eventlen - eventlen2 - eventlen3 - eventlen4 - eventlen5 - eventlen6 - eventlen7 - eventlen8 - eventlen9)
            data = lecroyparser.ScopeData(ReflectionInputPath6+InputDataName)
            self.TimeList = data.x.tolist()
            self.PreVoltList = data.y.tolist()
        if  eventlen + eventlen2 + eventlen3 + eventlen4 + eventlen5 + eventlen6 + eventlen7 + eventlen8 + eventlen9 + eventlen10 + eventlen11 - 1 < self.EventNumber < eventlen + eventlen2 + eventlen3 + eventlen4 + eventlen5 + eventlen6 + eventlen7 + eventlen8 + eventlen9 + eventlen10 + eventlen11 + eventlen12 + eventlen13:
            InputDataName = 'C%s--Trace--%05d.trc' %(self.ChannelId - 13, self.EventNumber - eventlen - eventlen2 - eventlen3 - eventlen4 - eventlen5 - eventlen6 - eventlen7 - eventlen8 - eventlen9 - eventlen10 - eventlen11)
            data = lecroyparser.ScopeData(ReflectionInputPath7+InputDataName)
            self.TimeList = data.x.tolist()
            self.PreVoltList = data.y.tolist()
        if  eventlen + eventlen2 + eventlen3 + eventlen4 + eventlen5 + eventlen6 + eventlen7 + eventlen8 + eventlen9 + eventlen10 + eventlen11 + eventlen12 + eventlen13 - 1 < self.EventNumber < eventlen + eventlen2 + eventlen3 + eventlen4 + eventlen5 + eventlen6 + eventlen7 + eventlen8 + eventlen9 + eventlen10 + eventlen11 + eventlen12 + eventlen13 + eventlen14 + eventlen15:
            InputDataName = 'C%s--Trace--%05d.trc' %(self.ChannelId - 13, self.EventNumber - eventlen - eventlen2 - eventlen3 - eventlen4 - eventlen5 - eventlen6 - eventlen7 - eventlen8 - eventlen9 - eventlen10 - eventlen11 - eventlen12 - eventlen13)
            data = lecroyparser.ScopeData(ReflectionInputPath8+InputDataName)
            self.TimeList = data.x.tolist()
            self.PreVoltList = data.y.tolist()
        if  eventlen + eventlen2 + eventlen3 + eventlen4 + eventlen5 + eventlen6 + eventlen7 + eventlen8 + eventlen9 + eventlen10 + eventlen11 + eventlen12 + eventlen13 + eventlen14 + eventlen15 - 1 < self.EventNumber < eventlen + eventlen2 + eventlen3 + eventlen4 + eventlen5 + eventlen6 + eventlen7 + eventlen8 + eventlen9 + eventlen10 + eventlen11 + eventlen12 + eventlen13 + eventlen14 + eventlen15 + eventlen16 + eventlen17:
            InputDataName = 'C%s--Trace--%05d.trc' %(self.ChannelId - 13, self.EventNumber - eventlen - eventlen2 - eventlen3 - eventlen4 - eventlen5 - eventlen6 - eventlen7 - eventlen8 - eventlen9 - eventlen10 - eventlen11 - eventlen12 - eventlen13 - eventlen14 - eventlen15)
            data = lecroyparser.ScopeData(ReflectionInputPath9+InputDataName)
            self.TimeList = data.x.tolist()
            self.PreVoltList = data.y.tolist()
        if  eventlen + eventlen2 + eventlen3 + eventlen4 + eventlen5 + eventlen6 + eventlen7 + eventlen8 + eventlen9 + eventlen10 + eventlen11 + eventlen12 + eventlen13 + eventlen14 + eventlen15 + eventlen16 + eventlen17 - 1 < self.EventNumber < eventlen + eventlen2 + eventlen3 + eventlen4 + eventlen5 + eventlen6 + eventlen7 + eventlen8 + eventlen9 + eventlen10 + eventlen11 + eventlen12 + eventlen13 + eventlen14 + eventlen15 + eventlen16 + eventlen17 + eventlen18 + eventlen19:
            InputDataName = 'C%s--Trace--%05d.trc' %(self.ChannelId - 13, self.EventNumber - eventlen - eventlen2 - eventlen3 - eventlen4 - eventlen5 - eventlen6 - eventlen7 - eventlen8 - eventlen9 - eventlen10 - eventlen11 - eventlen12 - eventlen13 - eventlen14 - eventlen15 - eventlen16 - eventlen17)
            data = lecroyparser.ScopeData(ReflectionInputPath10+InputDataName)
            self.TimeList = data.x.tolist()
            self.PreVoltList = data.y.tolist()
        self.ReflectionBinSize = len(self.TimeList)
        for index in range(self.ReflectionBinSize):
            self.TimeList[index] = self.TimeList[index]* 1000000000 + 400 
            self.PreVoltList[index] = self.PreVoltList[index] * 1000

    def ReflectionFFT(self):
        AmpArr = np.array(self.PreVoltList)
        FFTFreqArr = np.fft.fft(AmpArr)
        for i in range(self.ReflectionBinSize): 
            if i > 120 and i < 1882:
                FFTFreqArr[i] = 4.37518033e-15
        self.AfterFFTAmpArr = np.fft.ifft(FFTFreqArr)
        self.AfterFFTAmpArr = np.real(self.AfterFFTAmpArr)
        self.IsFFT = True
    
    def ReflectionAfterFFTRmsCalculate(self):
        sumvolt = 0
        sumnum = 0
        sumSquare = 0
        for i in range(200):
            sumvolt += self.AfterFFTAmpArr[i]
            sumnum += 1
        self.ReflectionBaselineValue = sumvolt / sumnum
        for i in range(200):
            sumSquare += math.pow((self.AfterFFTAmpArr[i]-self.ReflectionBaselineValue), 2)
        self.ReflectionRms = math.sqrt(sumSquare/sumnum)
        for i in range(len(self.TimeList)):
            self.CorVoltList.append(self.AfterFFTAmpArr[i] - self.ReflectionBaselineValue)
    
    def FindOriginalSignalPeak(self):
        FirstPeakValue = 0
        for index in range(1000,1200):
            if FirstPeakValue > self.CorVoltList[index]:
                FirstPeakValue = self.CorVoltList[index]
                FirstPeakTime = self.TimeList[index]
                if FirstPeakValue < - 7 * self.ReflectionRms and self.CorVoltList[index-2]> self.CorVoltList[index-1]> FirstPeakValue and self.CorVoltList[index+2]> self.CorVoltList[index+1]> FirstPeakValue :
                    self.ReflectionTimeArr[1] = FirstPeakTime
                    self.OrignalPeakBin = index
                    self.OrignalPeakValue = FirstPeakValue
    
    def OriginalSignalParaDetermination(self):
        self.Signal1LEThreshold = self.OrignalPeakValue * self.ReflectionLEThreshold    # LE/FE: 70%PeakValue
        if self.ReflectionTimeArr[1] > 0:
            for index in range(self.OrignalPeakBin,ReflectionSignalStartBin,-1):
                if  self.CorVoltList[index] > self.Signal1LEThreshold and self.CorVoltList[index+1] < self.CorVoltList[index] < self.CorVoltList[index-1]:
                    self.ReflectionTimeArr[0] = self.TimeList[index]
                    self.Signal1LEindex = index
                    break
            for index in range(self.OrignalPeakBin,ReflectionSignalEndBin):
                if  self.CorVoltList[index] > self.Signal1LEThreshold and self.CorVoltList[index+1] > self.CorVoltList[index] > self.CorVoltList[index-1]:
                    self.ReflectionTimeArr[2] = self.TimeList[index]
                    self.Signal1FEindex = index
                    break
            for index in range(self.Signal1FEindex,ReflectionSignalEndBin):
                if self.CorVoltList[index+2] < self.CorVoltList[index+1] < self.CorVoltList[index] and self.CorVoltList[index-2] < self.CorVoltList[index-1] < self.CorVoltList[index]:
                    self.Signal1Endindex = index
                    break
            for index in range(self.OrignalPeakBin,ReflectionSignalStartBin,-1):
                if  self.CorVoltList[index] > self.OrignalPeakValue * 0.5 and self.CorVoltList[index+1] < self.CorVoltList[index] < self.CorVoltList[index-1]:
                    self.Signal1LETime50 = self.TimeList[index]
                    self.Signal1LE50Bin = index
                    break
            for index in range(self.OrignalPeakBin,ReflectionSignalStartBin,-1):
                if  self.CorVoltList[index] > self.OrignalPeakValue * 0.6 and self.CorVoltList[index+1] < self.CorVoltList[index] < self.CorVoltList[index-1]:
                    self.Signal1LETime60 = self.TimeList[index]
                    self.Signal1LE60Bin = index
                    break
            for index in range(self.OrignalPeakBin,ReflectionSignalStartBin,-1):
                if  self.CorVoltList[index] > self.OrignalPeakValue * 0.8 and self.CorVoltList[index+1] < self.CorVoltList[index] < self.CorVoltList[index-1]:
                    self.Signal1LETime80 = self.TimeList[index]
                    self.Signal1LE80Bin = index
                    break
            for index in range(self.OrignalPeakBin,ReflectionSignalStartBin,-1):
                if  self.CorVoltList[index] > self.OrignalPeakValue * 0.9 and self.CorVoltList[index+1] < self.CorVoltList[index] < self.CorVoltList[index-1]:
                    self.Signal1LETime90 = self.TimeList[index]
                    self.Signal1LE90Bin = index
                    break

    def FindReflectedSignalPeak(self):
        if self.ReflectionTimeArr[1] > 0:
            SecondPeakValue = 0
            for index in range(self.Signal1Endindex,ReflectionSignalEndBin):
                if SecondPeakValue > self.CorVoltList[index]:
                    SecondPeakValue = self.CorVoltList[index]
                    SecondPeakTime = self.TimeList[index]
                    #if SecondPeakValue < -3 and 1 > (SecondPeakValue/self.OrignalPeakValue) > 0.6 and self.CorVoltList[index-2]> self.CorVoltList[index-1]> SecondPeakValue and self.CorVoltList[index+2]> self.CorVoltList[index+1]> SecondPeakValue :
                    if SecondPeakValue < - 5 * self.ReflectionRms and 1 > (SecondPeakValue/self.OrignalPeakValue) > 0.6 and self.CorVoltList[index-2]> self.CorVoltList[index-1]> SecondPeakValue and self.CorVoltList[index+2]> self.CorVoltList[index+1]> SecondPeakValue and 8.5 > (self.TimeList[index] - self.ReflectionTimeArr[1]) > 4 :
                        self.ReflectionTimeArr[4] = SecondPeakTime
                        self.ReflectedPeakValue = SecondPeakValue
                        self.ReflectedPeakBin = index
                        break

    def ReflectedSignalParaDetermination(self): 
        if self.ReflectionTimeArr[1] > 0:
            self.Signal2LEThreshold = self.ReflectedPeakValue * self.ReflectionLEThreshold
            for index in range(self.ReflectedPeakBin,self.Signal1Endindex,-1):
                if  self.CorVoltList[index] > self.Signal2LEThreshold and self.CorVoltList[index+2] < self.CorVoltList[index+1] < self.CorVoltList[index] < self.CorVoltList[index-1] < self.CorVoltList[index-2]:
                    self.ReflectionTimeArr[3] = self.TimeList[index]
                    self.Signal2LEindex = index
                    break
            for index in range(self.ReflectedPeakBin,ReflectionSignalEndBin):
                if self.CorVoltList[index] > self.Signal2LEThreshold and self.CorVoltList[index+2] > self.CorVoltList[index+1] > self.CorVoltList[index] > self.CorVoltList[index-1] > self.CorVoltList[index-2]:
                    self.ReflectionTimeArr[5] = self.TimeList[index]
                    self.Signal2FEindex = index
                    break
            for index in range(self.ReflectedPeakBin,self.Signal1Endindex,-1):
                if  self.CorVoltList[index] > self.ReflectedPeakValue * 0.5 and self.CorVoltList[index+2] < self.CorVoltList[index+1] < self.CorVoltList[index] < self.CorVoltList[index-1] < self.CorVoltList[index-2]:
                    self.Signal2LETime50 = self.TimeList[index]
                    self.Signal2LE50Bin = index
                    break
            for index in range(self.ReflectedPeakBin,self.Signal1Endindex,-1):
                if  self.CorVoltList[index] > self.ReflectedPeakValue * 0.6 and self.CorVoltList[index+2] < self.CorVoltList[index+1] < self.CorVoltList[index] < self.CorVoltList[index-1] < self.CorVoltList[index-2]:
                    self.Signal2LETime60 = self.TimeList[index]
                    self.Signal2LE60Bin = index
                    break
            for index in range(self.ReflectedPeakBin,self.Signal1Endindex,-1):
                if  self.CorVoltList[index] > self.ReflectedPeakValue * 0.8 and self.CorVoltList[index+2] < self.CorVoltList[index+1] < self.CorVoltList[index] < self.CorVoltList[index-1] < self.CorVoltList[index-2]:
                    self.Signal2LETime80 = self.TimeList[index]
                    self.Signal2LE80Bin = index
                    break
            for index in range(self.ReflectedPeakBin,self.Signal1Endindex,-1):
                if  self.CorVoltList[index] > self.ReflectedPeakValue * 0.9 and self.CorVoltList[index+2] < self.CorVoltList[index+1] < self.CorVoltList[index] < self.CorVoltList[index-1] < self.CorVoltList[index-2]:
                    self.Signal2LETime90 = self.TimeList[index]
                    self.Signal2LE90Bin = index
                    break

    def ReflectionTwoLEFineCal(self):
        Signal1LEupVolt = self.CorVoltList[self.Signal1LEindex]
        Signal1LEDownVolt = self.CorVoltList[self.Signal1LEindex + 1]
        Slope = (Signal1LEupVolt - Signal1LEDownVolt) / 0.1         # 单位: mV/ns
        Signal1VoltGap = Signal1LEupVolt - self.Signal1LEThreshold
        Signal1TimeGap = Signal1VoltGap / Slope
        self.ReflectionTimeArr[0] = self.ReflectionTimeArr[0] + Signal1TimeGap
        Signal2LEUpVolt = self.CorVoltList[self.Signal2LEindex]
        Signal2LEDownVolt = self.CorVoltList[self.Signal2LEindex - 1]
        Slope = (Signal2LEUpVolt - Signal2LEDownVolt) / 0.1         # 单位: mV/ns
        Signal2VoltGap = Signal2LEUpVolt - self.Signal2LEThreshold
        Signal2TimeGap = Signal2VoltGap / Slope
        self.ReflectionTimeArr[3] = self.ReflectionTimeArr[3] - Signal2TimeGap
        Signal1LEupVolt50 = self.CorVoltList[self.Signal1LE50Bin]
        Signal1LEDownVolt50 = self.CorVoltList[self.Signal1LE50Bin + 1]
        Slope50 = (Signal1LEupVolt50 - Signal1LEDownVolt50) / 0.1         # 单位: mV/ns
        Signal1VoltGap50 = Signal1LEupVolt50 - (self.OrignalPeakValue * 0.5)
        Signal1TimeGap50 = Signal1VoltGap50 / Slope50
        self.Signal1LETime50 = self.Signal1LETime50 + Signal1TimeGap50
        Signal2LEUpVolt50 = self.CorVoltList[self.Signal2LE50Bin]
        Signal2LEDownVolt50 = self.CorVoltList[self.Signal2LE50Bin - 1]
        Slope50 = (Signal2LEUpVolt50 - Signal2LEDownVolt50) / 0.1         # 单位: mV/ns
        Signal2VoltGap50 = Signal2LEUpVolt50 - (self.ReflectedPeakValue * 0.5)
        Signal2TimeGap50 = Signal2VoltGap50 / Slope50
        self.Signal2LETime50 = self.Signal2LETime50 - Signal2TimeGap50
        Signal1LEupVolt60 = self.CorVoltList[self.Signal1LE60Bin]
        Signal1LEDownVolt60 = self.CorVoltList[self.Signal1LE60Bin + 1]
        Slope60 = (Signal1LEupVolt60 - Signal1LEDownVolt60) / 0.1         # 单位: mV/ns
        Signal1VoltGap60 = Signal1LEupVolt60 - (self.OrignalPeakValue * 0.6)
        Signal1TimeGap60 = Signal1VoltGap60 / Slope60
        self.Signal1LETime60 = self.Signal1LETime60 + Signal1TimeGap60
        Signal2LEUpVolt60 = self.CorVoltList[self.Signal2LE60Bin]
        Signal2LEDownVolt60 = self.CorVoltList[self.Signal2LE60Bin - 1]
        Slope60 = (Signal2LEUpVolt60 - Signal2LEDownVolt60) / 0.1         # 单位: mV/ns
        Signal2VoltGap60 = Signal2LEUpVolt60 - (self.ReflectedPeakValue * 0.6)
        Signal2TimeGap60 = Signal2VoltGap60 / Slope60
        self.Signal2LETime60 = self.Signal2LETime60 - Signal2TimeGap60
        Signal1LEupVolt80 = self.CorVoltList[self.Signal1LE80Bin]
        Signal1LEDownVolt80 = self.CorVoltList[self.Signal1LE80Bin + 1]
        Slope80 = (Signal1LEupVolt80 - Signal1LEDownVolt80) / 0.1         # 单位: mV/ns
        Signal1VoltGap80 = Signal1LEupVolt80 - (self.OrignalPeakValue * 0.8)
        Signal1TimeGap80 = Signal1VoltGap80 / Slope80
        self.Signal1LETime80 = self.Signal1LETime80 + Signal1TimeGap80
        Signal2LEUpVolt80 = self.CorVoltList[self.Signal2LE80Bin]
        Signal2LEDownVolt80 = self.CorVoltList[self.Signal2LE80Bin - 1]
        Slope80 = (Signal2LEUpVolt80 - Signal2LEDownVolt80) / 0.1         # 单位: mV/ns
        Signal2VoltGap80 = Signal2LEUpVolt80 - (self.ReflectedPeakValue * 0.8)
        Signal2TimeGap80 = Signal2VoltGap80 / Slope80
        self.Signal2LETime80 = self.Signal2LETime80 - Signal2TimeGap80
        Signal1LEupVolt90 = self.CorVoltList[self.Signal1LE90Bin]
        Signal1LEDownVolt90 = self.CorVoltList[self.Signal1LE90Bin + 1]
        Slope90 = (Signal1LEupVolt90 - Signal1LEDownVolt90) / 0.1         # 单位: mV/ns
        Signal1VoltGap90 = Signal1LEupVolt90 - (self.OrignalPeakValue * 0.9)
        Signal1TimeGap90 = Signal1VoltGap90 / Slope90
        self.Signal1LETime90 = self.Signal1LETime90 + Signal1TimeGap90
        Signal2LEUpVolt90 = self.CorVoltList[self.Signal2LE90Bin]
        Signal2LEDownVolt90 = self.CorVoltList[self.Signal2LE90Bin - 1]
        Slope90 = (Signal2LEUpVolt90 - Signal2LEDownVolt90) / 0.1         # 单位: mV/ns
        Signal2VoltGap90 = Signal2LEUpVolt90 - (self.ReflectedPeakValue * 0.9)
        Signal2TimeGap90 = Signal2VoltGap90 / Slope90
        self.Signal2LETime90 = self.Signal2LETime90 - Signal2TimeGap90
        
    def Timediff(self):
        self.TimeGap = self.ReflectionTimeArr[4] - self.ReflectionTimeArr[1]
        self.TimeGap50 = self.Signal2LETime50 - self.Signal1LETime50
        self.TimeGap60 = self.Signal2LETime60 - self.Signal1LETime60
        self.TimeGap70 = self.ReflectionTimeArr[3] - self.ReflectionTimeArr[0]
        self.TimeGap80 = self.Signal2LETime80 - self.Signal1LETime80
        self.TimeGap90 = self.Signal2LETime90 - self.Signal1LETime90

    def IsReflectedChannelHit(self):
        if self.ReflectionTimeArr[4] > self.ReflectionTimeArr[1] > 0 and self.OrignalPeakValue < self.ReflectedPeakValue < 0 and (self.ReflectedPeakValue/self.OrignalPeakValue) > 0.6 :
            self.ReflectionIsSignal = True

    def ReflectionWaveformDrawing(self):
        c1 = TCanvas("c", "BPRE", 800, 600)
        c1.cd()
        c1.Divide(1,1,0.008,0.007)
        gPad.SetTopMargin(0.09)
        gPad.SetBottomMargin(0.10)
        gPad.SetLeftMargin(0.10)
        gPad.SetRightMargin(0.05)
        TimeArr = array('f',self.TimeList)
        VoltArr = array('f',self.CorVoltList)
        WaveformGraph = TGraph(len(self.TimeList),TimeArr,VoltArr) 
        WaveformGraph.SetLineColor(1)
        WaveformGraph.SetLineWidth(2)
        WaveformGraph.Draw("AL")
        WaveformGraph.SetTitle("Event "+str(self.EventNumber) + "_Reflection_Strip "+str(self.ChannelId-14)+"_Waveform")
        ax = WaveformGraph.GetXaxis()
        ax.SetRangeUser(250, 290)
        ax.SetTitle( "Time [ns]" )
        ay = WaveformGraph.GetYaxis()
        ay.SetTitle( "Amplitude [mV]" )
        ay.SetRangeUser(self.OrignalPeakValue - 5, 5)
        ay.SetTitleSize(0.04)
        ax.SetTitleOffset(1.0)
        ay.SetTitleOffset(0.8)
        ax.SetTitleSize(0.04)
        ax.SetLabelSize(0.04)
        ay.SetLabelSize(0.04)
        ax.Draw("same")
        ay.Draw("same")
        c1.SaveAs("Event "+str(self.EventNumber) + "_Reflection_Strip "+str(self.ChannelId-14)+"_Waveform.png")

        



