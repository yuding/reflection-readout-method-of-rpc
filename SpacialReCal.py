from ROOT import *
from ParaAndDataInput import *
from Channel import *
from Event import *
import math

c3 = TCanvas("c3", "BPRE", 800, 600)
c3.cd()
c3.Divide(1,1,0.008,0.007)
gPad.SetTopMargin(0.09)
gPad.SetBottomMargin(0.10)
gPad.SetLeftMargin(0.10)
gPad.SetRightMargin(0.05)
EventNum = eventlen + eventlen2 + eventlen3 + eventlen4 +  eventlen5 + eventlen6 + eventlen7 + eventlen8 + eventlen9 + eventlen10 + eventlen11 + eventlen12 + eventlen13 + eventlen14 + eventlen15 + eventlen16 + eventlen17 + eventlen18 + eventlen19
SpacialReS2Dis = TH1F("SpacialResolutionS2","SpacialResolutionS2",30,-60,60)
SpacialReS3Dis = TH1F("SpacialResolutionS3","SpacialResolutionS3",30,-60,60)
SpacialReS4Dis = TH1F("SpacialResolutionS4","SpacialResolutionS4",30,-60,60)
SpacialReS5Dis = TH1F("SpacialResolutionS5","SpacialResolutionS5",30,-60,60)
SpaceResS250Dis = TH1F("SpacialResoS2_50%","SpacialResoS2_50%",30,-60,60)
SpaceResS260Dis = TH1F("SpacialResoS2_60%","SpacialResoS2_60%",30,-60,60)
SpaceResS270Dis = TH1F("SpacialResoS2_70%","SpacialResoS2_70%",30,-60,60)
SpaceResS280Dis = TH1F("SpacialResoS2_80%","SpacialResoS2_80%",30,-60,60)
SpaceResS290Dis = TH1F("SpacialResoS2_90%","SpacialResoS2_90%",30,-60,60)
SpaceResS350Dis = TH1F("SpacialResoS3_50%","SpacialResoS3_50%",30,-60,60)
SpaceResS360Dis = TH1F("SpacialResoS3_60%","SpacialResoS3_60%",30,-60,60)
SpaceResS370Dis = TH1F("SpacialResoS3_70%","SpacialResoS3_70%",30,-60,60)
SpaceResS380Dis = TH1F("SpacialResoS3_80%","SpacialResoS3_80%",30,-60,60)
SpaceResS390Dis = TH1F("SpacialResoS3_90%","SpacialResoS3_90%",30,-60,60)
SpaceResS450Dis = TH1F("SpacialResoS4_50%","SpacialResoS4_50%",30,-60,60)
SpaceResS460Dis = TH1F("SpacialResoS4_60%","SpacialResoS4_60%",30,-60,60)
SpaceResS470Dis = TH1F("SpacialResoS4_70%","SpacialResoS4_70%",30,-60,60)
SpaceResS480Dis = TH1F("SpacialResoS4_80%","SpacialResoS4_80%",30,-60,60)
SpaceResS490Dis = TH1F("SpacialResoS4_90%","SpacialResoS4_90%",30,-60,60)
SpaceResS550Dis = TH1F("SpacialResoS5_50%","SpacialResoS5_50%",30,-60,60)
SpaceResS560Dis = TH1F("SpacialResoS5_60%","SpacialResoS5_60%",30,-60,60)
SpaceResS570Dis = TH1F("SpacialResoS5_70%","SpacialResoS5_70%",30,-60,60)
SpaceResS580Dis = TH1F("SpacialResoS5_80%","SpacialResoS5_80%",30,-60,60)
SpaceResS590Dis = TH1F("SpacialResoS5_90%","SpacialResoS5_90%",30,-60,60)

P1S1 = 8.352
P1S1Err = 0.043
P2S1 =  -0.2503
P2S1Err = 0.0047
P1S2 = 8.42
P1S2Err = 0.037
P2S2 = -0.2550
P2S2Err = 0.0039
P1S3 = 8.399
P1S3Err = 0.033
P2S3 =  -0.2542
P2S3Err = 0.0036
P1S4 = 8.445
P1S4Err = 0.033
P2S4 =  -0.2562
P2S4Err = 0.0036
P1S5 = 8.496
P1S5Err = 0.033
P2S5 = -0.2600
P2S5Err = 0.0036
P1S6 = 8.444
P1S6Err = 0.068
P2S6 = -0.2526
P2S6Err = 0.0075

P1S150 = 8.174
P1S1Err50 = 0.040
P2S150 =  -0.2409
P2S1Err50 = 0.0044
P1S250 = 8.23
P1S2Err50 = 0.038
P2S250 = -0.2442
P2S2Err50 = 0.0041
P1S350 = 8.223
P1S3Err50 = 0.035
P2S350 =  -0.2440
P2S3Err50 = 0.0038
P1S450 = 8.2857
P1S4Err50 = 0.034
P2S450 =  -0.2477
P2S4Err50 = 0.0037
P1S550 = 8.342
P1S5Err50 = 0.034
P2S550 = -0.2535
P2S5Err50 = 0.0037
P1S650 = 8.253
P1S6Err50 = 0.066
P2S650 = -0.2438
P2S6Err50 = 0.0072

P1S160 = 8.191
P1S1Err60 = 0.040
P2S160 =  -0.2424
P2S1Err60 = 0.0044
P1S260 = 8.24
P1S2Err60 = 0.038
P2S260 = -0.2452
P2S2Err60 = 0.0040
P1S360 = 8.235
P1S3Err60 = 0.034
P2S360 =  -0.2451
P2S3Err60 = 0.0037
P1S460 = 8.297
P1S4Err60 = 0.033
P2S460 =  -0.2488
P2S4Err60 = 0.0036
P1S560 = 8.354
P1S5Err60 = 0.034
P2S560 = -0.2543
P2S5Err60 = 0.0037
P1S660 = 8.273
P1S6Err60 = 0.067
P2S660 = -0.2447
P2S6Err60 = 0.0074

P1S170 = 8.212
P1S1Err70 = 0.040
P2S170 =  -0.2443
P2S1Err70 = 0.0045
P1S270 = 8.256
P1S2Err70 = 0.037
P2S270 = -0.2466
P2S2Err70 = 0.0040
P1S370 = 8.254
P1S3Err70 = 0.033
P2S370 =  -0.2469
P2S3Err70 = 0.0037
P1S470 = 8.310
P1S4Err70 = 0.032
P2S470 =  -0.2500
P2S4Err70 = 0.0035
P1S570 = 8.362
P1S5Err70 = 0.033
P2S570 = -0.2547
P2S5Err70 = 0.0036
P1S670 = 8.292
P1S6Err70 = 0.068
P2S670 = -0.2461
P2S6Err70 = 0.0073

P1S180 = 8.230
P1S1Err80 = 0.041
P2S180 =  -0.2458
P2S1Err80 = 0.0046
P1S280 = 8.274
P1S2Err80 = 0.037
P2S280 = -0.2481
P2S2Err80 = 0.0040
P1S380 = 8.273
P1S3Err80 = 0.033
P2S380 =  -0.2487
P2S3Err80 = 0.0036
P1S480 = 8.322
P1S4Err80 = 0.033
P2S480 =  -0.2510
P2S4Err80 = 0.0035
P1S580 = 8.374
P1S5Err80 = 0.033
P2S580 = -0.2555
P2S5Err80 = 0.0036
P1S680 = 8.310
P1S6Err80 = 0.067
P2S680 = -0.2473
P2S6Err80 = 0.0074

P1S190 = 8.258
P1S1Err90 = 0.041
P2S190 =  -0.2477
P2S1Err90 = 0.0045
P1S290 = 8.295
P1S2Err90 = 0.037
P2S290 = -0.2498
P2S2Err90 = 0.0040
P1S390 = 8.293
P1S3Err90 = 0.032
P2S390 =  -0.2505
P2S3Err90 = 0.0035
P1S490 = 8.341
P1S4Err90 = 0.033
P2S490 =  -0.2526
P2S4Err90 = 0.0035
P1S590 = 8.389
P1S5Err90 = 0.033
P2S590 = -0.2564
P2S5Err90 = 0.0035
P1S690 = 8.331
P1S6Err90 = 0.068
P2S690 = -0.2487
P2S6Err90 = 0.0074

for eventindex in range(EventNum):
#for eventindex in range(3000):
    event = Event(eventindex)
    event.MuonBundleDetermination()
    event.ReflectionFindMorePeak()
    if event.IsHit == True and event.IsMuonBundle == False:
        if event.ReflectionOnlyTwoPeak == True:
            ReferenceHitChannel = event.ReferenceHitChannel
            ReflectionHitChannel = event.ReflectionHitChannel
            if ReflectionHitChannel == 2 and 0 < ReferenceHitChannel < 15:
                ReferencePosS2 = (ReferenceHitChannel * 2.7) + (2.7/2)
                TimeGapS2 = event.ReflectionChannelList[1].ReflectionTimeArr[4] - event.ReflectionChannelList[1].ReflectionTimeArr[1]
                TimeGapS250 = event.ReflectionChannelList[1].TimeGap50
                TimeGapS260 = event.ReflectionChannelList[1].TimeGap60
                TimeGapS270 = event.ReflectionChannelList[1].TimeGap70
                TimeGapS280 = event.ReflectionChannelList[1].TimeGap80
                TimeGapS290 = event.ReflectionChannelList[1].TimeGap90
                ReflectionPosS2 = ((P1S2 - TimeGapS2) / (-P2S2)) * 2.7
                ReflectionPosS250 = ((P1S250 - TimeGapS250) / (-P2S250)) * 2.7
                ReflectionPosS260 = ((P1S260 - TimeGapS260) / (-P2S260)) * 2.7
                ReflectionPosS270 = ((P1S270 - TimeGapS270) / (-P2S270)) * 2.7
                ReflectionPosS280 = ((P1S280 - TimeGapS280) / (-P2S280)) * 2.7
                ReflectionPosS290 = ((P1S290 - TimeGapS290) / (-P2S290)) * 2.7
                PosGapS2 = (ReflectionPosS2 - ReferencePosS2) * 10     #mm
                PosGapS250 = (ReflectionPosS250 - ReferencePosS2) * 10 
                PosGapS260 = (ReflectionPosS260 - ReferencePosS2) * 10 
                PosGapS270 = (ReflectionPosS270 - ReferencePosS2) * 10 
                PosGapS280 = (ReflectionPosS280 - ReferencePosS2) * 10 
                PosGapS290 = (ReflectionPosS290 - ReferencePosS2) * 10  
                SpacialReS2Dis.Fill(PosGapS2)
                SpaceResS250Dis.Fill(PosGapS250)
                SpaceResS260Dis.Fill(PosGapS260)
                SpaceResS270Dis.Fill(PosGapS270)
                SpaceResS280Dis.Fill(PosGapS280)
                SpaceResS290Dis.Fill(PosGapS290)
            if ReflectionHitChannel == 3 and 0 < ReferenceHitChannel < 15:
                ReferencePosS3 = (ReferenceHitChannel * 2.7) + (2.7/2)
                TimeGapS3 = event.ReflectionChannelList[2].ReflectionTimeArr[4] - event.ReflectionChannelList[2].ReflectionTimeArr[1]
                TimeGapS350 = event.ReflectionChannelList[2].TimeGap50
                TimeGapS360 = event.ReflectionChannelList[2].TimeGap60
                TimeGapS370 = event.ReflectionChannelList[2].TimeGap70
                TimeGapS380 = event.ReflectionChannelList[2].TimeGap80
                TimeGapS390 = event.ReflectionChannelList[2].TimeGap90
                ReflectionPosS3 = ((P1S3 - TimeGapS3) / (-P2S3)) * 2.7
                ReflectionPosS350 = ((P1S350 - P1S3Err50 - TimeGapS350) / (-P2S350)) * 2.7
                ReflectionPosS360 = ((P1S360 - P1S3Err60 - TimeGapS360) / (-P2S360)) * 2.7
                ReflectionPosS370 = ((P1S370 + P1S3Err70 - TimeGapS370) / (-P2S370 - P2S3Err70)) * 2.7
                ReflectionPosS380 = ((P1S380 - P1S3Err80 - TimeGapS380) / (-P2S380)) * 2.7
                ReflectionPosS390 = ((P1S390 - P1S3Err90 - TimeGapS390) / (-P2S390)) * 2.7
                PosGapS3 = (ReflectionPosS3 - ReferencePosS3) * 10     #mm
                PosGapS350 = (ReflectionPosS350 - ReferencePosS3) * 10 
                PosGapS360 = (ReflectionPosS360 - ReferencePosS3) * 10 
                PosGapS370 = (ReflectionPosS370 - ReferencePosS3) * 10 
                PosGapS380 = (ReflectionPosS380 - ReferencePosS3) * 10 
                PosGapS390 = (ReflectionPosS390 - ReferencePosS3) * 10  
                SpacialReS3Dis.Fill(PosGapS3)
                SpaceResS350Dis.Fill(PosGapS350)
                SpaceResS360Dis.Fill(PosGapS360)
                SpaceResS370Dis.Fill(PosGapS370)
                SpaceResS380Dis.Fill(PosGapS380)
                SpaceResS390Dis.Fill(PosGapS390)
            if ReflectionHitChannel == 4 and 0 < ReferenceHitChannel < 15:
                ReferencePosS4 = (ReferenceHitChannel * 2.7) + (2.7/2)
                TimeGapS4 = event.ReflectionChannelList[3].ReflectionTimeArr[4] - event.ReflectionChannelList[3].ReflectionTimeArr[1]
                TimeGapS450 = event.ReflectionChannelList[3].TimeGap50
                TimeGapS460 = event.ReflectionChannelList[3].TimeGap60
                TimeGapS470 = event.ReflectionChannelList[3].TimeGap70
                TimeGapS480 = event.ReflectionChannelList[3].TimeGap80
                TimeGapS490 = event.ReflectionChannelList[3].TimeGap90
                ReflectionPosS4 = ((P1S4 - TimeGapS4) / (-P2S4)) * 2.7
                ReflectionPosS450 = ((P1S450 - P1S4Err50 - TimeGapS450) / (-P2S450)) * 2.7
                ReflectionPosS460 = ((P1S460 - P1S4Err60 - TimeGapS460) / (-P2S460)) * 2.7
                ReflectionPosS470 = ((P1S470 + P1S4Err70 - TimeGapS470) / (-P2S470 - P2S4Err70)) * 2.7
                ReflectionPosS480 = ((P1S480 - P1S4Err80 - TimeGapS480) / (-P2S480)) * 2.7
                ReflectionPosS490 = ((P1S490 - P1S4Err90 - TimeGapS490) / (-P2S490)) * 2.7
                PosGapS4 = (ReflectionPosS4 - ReferencePosS4) * 10     #mm
                PosGapS450 = (ReflectionPosS450 - ReferencePosS4) * 10 
                PosGapS460 = (ReflectionPosS460 - ReferencePosS4) * 10 
                PosGapS470 = (ReflectionPosS470 - ReferencePosS4) * 10 
                PosGapS480 = (ReflectionPosS480 - ReferencePosS4) * 10 
                PosGapS490 = (ReflectionPosS490 - ReferencePosS4) * 10  
                SpacialReS4Dis.Fill(PosGapS4)
                SpaceResS450Dis.Fill(PosGapS450)
                SpaceResS460Dis.Fill(PosGapS460)
                SpaceResS470Dis.Fill(PosGapS470)
                SpaceResS480Dis.Fill(PosGapS480)
                SpaceResS490Dis.Fill(PosGapS490)
            if ReflectionHitChannel == 5 and 0 < ReferenceHitChannel < 15:
                ReferencePosS5 = (ReferenceHitChannel * 2.7) + (2.7/2)
                TimeGapS5 = event.ReflectionChannelList[4].ReflectionTimeArr[4] - event.ReflectionChannelList[4].ReflectionTimeArr[1]
                TimeGapS550 = event.ReflectionChannelList[4].TimeGap50
                TimeGapS560 = event.ReflectionChannelList[4].TimeGap60
                TimeGapS570 = event.ReflectionChannelList[4].TimeGap70
                TimeGapS580 = event.ReflectionChannelList[4].TimeGap80
                TimeGapS590 = event.ReflectionChannelList[4].TimeGap90
                ReflectionPosS5 = ((P1S5 - TimeGapS5) / (-P2S5)) * 2.7
                ReflectionPosS550 = ((P1S550 - P1S5Err50 - TimeGapS550) / (-P2S550)) * 2.7
                ReflectionPosS560 = ((P1S560 - P1S5Err60 - TimeGapS560) / (-P2S560)) * 2.7
                ReflectionPosS570 = ((P1S570 + P1S5Err70 - TimeGapS570) / (-P2S570 - P2S5Err70)) * 2.7
                ReflectionPosS580 = ((P1S580 - P1S5Err80 - TimeGapS580) / (-P2S580)) * 2.7
                ReflectionPosS590 = ((P1S590 - P1S5Err90 - TimeGapS590) / (-P2S590)) * 2.7
                PosGapS5 = (ReflectionPosS5 - ReferencePosS5) * 10     #mm
                PosGapS550 = (ReflectionPosS550 - ReferencePosS5) * 10 
                PosGapS560 = (ReflectionPosS560 - ReferencePosS5) * 10 
                PosGapS570 = (ReflectionPosS570 - ReferencePosS5) * 10 
                PosGapS580 = (ReflectionPosS580 - ReferencePosS5) * 10 
                PosGapS590 = (ReflectionPosS590 - ReferencePosS5) * 10  
                SpacialReS5Dis.Fill(PosGapS5)
                SpaceResS550Dis.Fill(PosGapS550)
                SpaceResS560Dis.Fill(PosGapS560)
                SpaceResS570Dis.Fill(PosGapS570)
                SpaceResS580Dis.Fill(PosGapS580)
                SpaceResS590Dis.Fill(PosGapS590)

SpacialReS2Dis.SetLineColor(kBlue)
resultS2 = SpacialReS2Dis.Fit("gaus","S")
chi2 = resultS2.Chi2()
fitNDof = resultS2.Ndf()
chiresult =  chi2/fitNDof
pvalue = resultS2.Prob()
print("Chiresult: ",chiresult,"pvalue: ",pvalue)
SpacialReS2Dis.Draw()
ax =SpacialReS2Dis.GetXaxis()
#ax.SetRangeUser(SignalStartBin, SignalEndBin)
ax.SetTitle( " PosGap(mm) " )
ay = SpacialReS2Dis.GetYaxis()
ay.SetTitle( "Entries" )
ay.SetTitleSize(0.04)
ax.SetTitleOffset(1.0)
ay.SetTitleOffset(0.8)
ax.SetTitleSize(0.04)
ax.SetLabelSize(0.04)
ay.SetLabelSize(0.04)
ax.Draw("same")
ay.Draw("same")
c3.SaveAs("SpacialReS2Dis.png")

SpaceResS250Dis.SetLineColor(kBlue)
resultS2 = SpaceResS250Dis.Fit("gaus","S")
chi2S2 = resultS2.Chi2()
fitNDofS2 = resultS2.Ndf()
chiresultS2 =  chi2S2/fitNDofS2
pvalueS2 = resultS2.Prob()
print("ChiresultS2: ",chiresultS2,"pvalue: ",pvalueS2)
SpaceResS250Dis.Draw()
ax =SpaceResS250Dis.GetXaxis()
#ax.SetRangeUser(SignalStartBin, SignalEndBin)
ax.SetTitle( " PosGap(mm) " )
ay = SpaceResS250Dis.GetYaxis()
ay.SetTitle( "Entries" )
ay.SetTitleSize(0.04)
ax.SetTitleOffset(1.0)
ay.SetTitleOffset(0.8)
ax.SetTitleSize(0.04)
ax.SetLabelSize(0.04)
ay.SetLabelSize(0.04)
ax.Draw("same")
ay.Draw("same")
c3.SaveAs("SpaceResS250Dis.png")

SpaceResS260Dis.SetLineColor(kBlue)
resultS2 = SpaceResS260Dis.Fit("gaus","S")
chi2S2 = resultS2.Chi2()
fitNDofS2 = resultS2.Ndf()
chiresultS2 =  chi2S2/fitNDofS2
pvalueS2 = resultS2.Prob()
print("ChiresultS2: ",chiresultS2,"pvalue: ",pvalueS2)
SpaceResS260Dis.Draw()
ax =SpaceResS260Dis.GetXaxis()
#ax.SetRangeUser(SignalStartBin, SignalEndBin)
ax.SetTitle( " PosGap(mm) " )
ay = SpaceResS260Dis.GetYaxis()
ay.SetTitle( "Entries" )
ay.SetTitleSize(0.04)
ax.SetTitleOffset(1.0)
ay.SetTitleOffset(0.8)
ax.SetTitleSize(0.04)
ax.SetLabelSize(0.04)
ay.SetLabelSize(0.04)
ax.Draw("same")
ay.Draw("same")
c3.SaveAs("SpaceResS260Dis.png")

SpaceResS270Dis.SetLineColor(kBlue)
resultS2 = SpaceResS270Dis.Fit("gaus","S")
chi2S2 = resultS2.Chi2()
fitNDofS2 = resultS2.Ndf()
chiresultS2 =  chi2S2/fitNDofS2
pvalueS2 = resultS2.Prob()
print("ChiresultS2: ",chiresultS2,"pvalue: ",pvalueS2)
SpaceResS270Dis.Draw()
ax =SpaceResS270Dis.GetXaxis()
#ax.SetRangeUser(SignalStartBin, SignalEndBin)
ax.SetTitle( " PosGap(mm) " )
ay = SpaceResS270Dis.GetYaxis()
ay.SetTitle( "Entries" )
ay.SetTitleSize(0.04)
ax.SetTitleOffset(1.0)
ay.SetTitleOffset(0.8)
ax.SetTitleSize(0.04)
ax.SetLabelSize(0.04)
ay.SetLabelSize(0.04)
ax.Draw("same")
ay.Draw("same")
c3.SaveAs("SpaceResS270Dis.png")

SpaceResS280Dis.SetLineColor(kBlue)
resultS2 = SpaceResS280Dis.Fit("gaus","S")
chi2S2 = resultS2.Chi2()
fitNDofS2 = resultS2.Ndf()
chiresultS2 =  chi2S2/fitNDofS2
pvalueS2 = resultS2.Prob()
print("ChiresultS2: ",chiresultS2,"pvalue: ",pvalueS2)
SpaceResS280Dis.Draw()
ax =SpaceResS280Dis.GetXaxis()
#ax.SetRangeUser(SignalStartBin, SignalEndBin)
ax.SetTitle( " PosGap(mm) " )
ay = SpaceResS280Dis.GetYaxis()
ay.SetTitle( "Entries" )
ay.SetTitleSize(0.04)
ax.SetTitleOffset(1.0)
ay.SetTitleOffset(0.8)
ax.SetTitleSize(0.04)
ax.SetLabelSize(0.04)
ay.SetLabelSize(0.04)
ax.Draw("same")
ay.Draw("same")
c3.SaveAs("SpaceResS280Dis.png")

SpaceResS290Dis.SetLineColor(kBlue)
resultS2 = SpaceResS290Dis.Fit("gaus","S")
chi2S2 = resultS2.Chi2()
fitNDofS2 = resultS2.Ndf()
chiresultS2 =  chi2S2/fitNDofS2
pvalueS2 = resultS2.Prob()
print("ChiresultS2: ",chiresultS2,"pvalue: ",pvalueS2)
SpaceResS290Dis.Draw()
ax =SpaceResS290Dis.GetXaxis()
#ax.SetRangeUser(SignalStartBin, SignalEndBin)
ax.SetTitle( " PosGap(mm) " )
ay = SpaceResS290Dis.GetYaxis()
ay.SetTitle( "Entries" )
ay.SetTitleSize(0.04)
ax.SetTitleOffset(1.0)
ay.SetTitleOffset(0.8)
ax.SetTitleSize(0.04)
ax.SetLabelSize(0.04)
ay.SetLabelSize(0.04)
ax.Draw("same")
ay.Draw("same")
c3.SaveAs("SpaceResS290Dis.png")

SpacialReS3Dis.SetLineColor(kBlue)
resultS3 = SpacialReS3Dis.Fit("gaus","S")
chi2S3 = resultS3.Chi2()
fitNDofS3 = resultS3.Ndf()
chiresultS3 =  chi2S3/fitNDofS3
pvalueS3 = resultS3.Prob()
print("ChiresultS3: ",chiresultS3,"pvalue: ",pvalueS3)
SpacialReS3Dis.Draw()
ax =SpacialReS3Dis.GetXaxis()
#ax.SetRangeUser(SignalStartBin, SignalEndBin)
ax.SetTitle( " PosGap(mm) " )
ay = SpacialReS3Dis.GetYaxis()
ay.SetTitle( "Entries" )
ay.SetTitleSize(0.04)
ax.SetTitleOffset(1.0)
ay.SetTitleOffset(0.8)
ax.SetTitleSize(0.04)
ax.SetLabelSize(0.04)
ay.SetLabelSize(0.04)
ax.Draw("same")
ay.Draw("same")
c3.SaveAs("SpacialReS3Dis.png")

SpaceResS350Dis.SetLineColor(kBlue)
resultS3 = SpaceResS350Dis.Fit("gaus","S")
chi2S3 = resultS3.Chi2()
fitNDofS3 = resultS3.Ndf()
chiresultS3 =  chi2S3/fitNDofS3
pvalueS3 = resultS3.Prob()
print("ChiresultS3: ",chiresultS3,"pvalue: ",pvalueS3)
SpaceResS350Dis.Draw()
ax =SpaceResS350Dis.GetXaxis()
#ax.SetRangeUser(SignalStartBin, SignalEndBin)
ax.SetTitle( " PosGap(mm) " )
ay = SpaceResS350Dis.GetYaxis()
ay.SetTitle( "Entries" )
ay.SetTitleSize(0.04)
ax.SetTitleOffset(1.0)
ay.SetTitleOffset(0.8)
ax.SetTitleSize(0.04)
ax.SetLabelSize(0.04)
ay.SetLabelSize(0.04)
ax.Draw("same")
ay.Draw("same")
c3.SaveAs("SpaceResS350Dis.png")

SpaceResS360Dis.SetLineColor(kBlue)
resultS3 = SpaceResS360Dis.Fit("gaus","S")
chi2S3 = resultS3.Chi2()
fitNDofS3 = resultS3.Ndf()
chiresultS3 =  chi2S3/fitNDofS3
pvalueS3 = resultS3.Prob()
print("ChiresultS3: ",chiresultS3,"pvalue: ",pvalueS3)
SpaceResS360Dis.Draw()
ax =SpaceResS360Dis.GetXaxis()
#ax.SetRangeUser(SignalStartBin, SignalEndBin)
ax.SetTitle( " PosGap(mm) " )
ay = SpaceResS360Dis.GetYaxis()
ay.SetTitle( "Entries" )
ay.SetTitleSize(0.04)
ax.SetTitleOffset(1.0)
ay.SetTitleOffset(0.8)
ax.SetTitleSize(0.04)
ax.SetLabelSize(0.04)
ay.SetLabelSize(0.04)
ax.Draw("same")
ay.Draw("same")
c3.SaveAs("SpaceResS360Dis.png")

SpaceResS370Dis.SetLineColor(kBlue)
resultS3 = SpaceResS370Dis.Fit("gaus","S")
chi2S3 = resultS3.Chi2()
fitNDofS3 = resultS3.Ndf()
chiresultS3 =  chi2S3/fitNDofS3
pvalueS3 = resultS3.Prob()
print("ChiresultS3: ",chiresultS3,"pvalue: ",pvalueS3)
SpaceResS370Dis.Draw()
ax =SpaceResS370Dis.GetXaxis()
#ax.SetRangeUser(SignalStartBin, SignalEndBin)
ax.SetTitle( " PosGap(mm) " )
ay = SpaceResS370Dis.GetYaxis()
ay.SetTitle( "Entries" )
ay.SetTitleSize(0.04)
ax.SetTitleOffset(1.0)
ay.SetTitleOffset(0.8)
ax.SetTitleSize(0.04)
ax.SetLabelSize(0.04)
ay.SetLabelSize(0.04)
ax.Draw("same")
ay.Draw("same")
c3.SaveAs("SpaceResS370Dis.png")

SpaceResS380Dis.SetLineColor(kBlue)
resultS3 = SpaceResS380Dis.Fit("gaus","S")
chi2S3 = resultS3.Chi2()
fitNDofS3 = resultS3.Ndf()
chiresultS3 =  chi2S3/fitNDofS3
pvalueS3 = resultS3.Prob()
print("ChiresultS3: ",chiresultS3,"pvalue: ",pvalueS3)
SpaceResS380Dis.Draw()
ax =SpaceResS380Dis.GetXaxis()
#ax.SetRangeUser(SignalStartBin, SignalEndBin)
ax.SetTitle( " PosGap(mm) " )
ay = SpaceResS380Dis.GetYaxis()
ay.SetTitle( "Entries" )
ay.SetTitleSize(0.04)
ax.SetTitleOffset(1.0)
ay.SetTitleOffset(0.8)
ax.SetTitleSize(0.04)
ax.SetLabelSize(0.04)
ay.SetLabelSize(0.04)
ax.Draw("same")
ay.Draw("same")
c3.SaveAs("SpaceResS380Dis.png")

SpaceResS390Dis.SetLineColor(kBlue)
resultS3 = SpaceResS390Dis.Fit("gaus","S")
chi2S3 = resultS3.Chi2()
fitNDofS3 = resultS3.Ndf()
chiresultS3 =  chi2S3/fitNDofS3
pvalueS3 = resultS3.Prob()
print("ChiresultS3: ",chiresultS3,"pvalue: ",pvalueS3)
SpaceResS390Dis.Draw()
ax =SpaceResS390Dis.GetXaxis()
#ax.SetRangeUser(SignalStartBin, SignalEndBin)
ax.SetTitle( " PosGap(mm) " )
ay = SpaceResS390Dis.GetYaxis()
ay.SetTitle( "Entries" )
ay.SetTitleSize(0.04)
ax.SetTitleOffset(1.0)
ay.SetTitleOffset(0.8)
ax.SetTitleSize(0.04)
ax.SetLabelSize(0.04)
ay.SetLabelSize(0.04)
ax.Draw("same")
ay.Draw("same")
c3.SaveAs("SpaceResS390Dis.png")

SpacialReS4Dis.SetLineColor(kBlue)
resultS4 = SpacialReS4Dis.Fit("gaus","S")
chi2S4 = resultS4.Chi2()
fitNDofS4 = resultS4.Ndf()
chiresultS4 =  chi2S4/fitNDofS4
pvalueS4 = resultS4.Prob()
print("ChiresultS4: ",chiresultS4,"pvalue: ",pvalueS4)
SpacialReS4Dis.Draw()
ax =SpacialReS4Dis.GetXaxis()
#ax.SetRangeUser(SignalStartBin, SignalEndBin)
ax.SetTitle( " PosGap(mm) " )
ay = SpacialReS4Dis.GetYaxis()
ay.SetTitle( "Entries" )
ay.SetTitleSize(0.04)
ax.SetTitleOffset(1.0)
ay.SetTitleOffset(0.8)
ax.SetTitleSize(0.04)
ax.SetLabelSize(0.04)
ay.SetLabelSize(0.04)
ax.Draw("same")
ay.Draw("same")
c3.SaveAs("SpacialReS4Dis.png")

SpaceResS450Dis.SetLineColor(kBlue)
resultS4 = SpaceResS450Dis.Fit("gaus","S")
chi2S4 = resultS4.Chi2()
fitNDofS4 = resultS4.Ndf()
chiresultS4 =  chi2S4/fitNDofS4
pvalueS4 = resultS4.Prob()
print("ChiresultS4: ",chiresultS4,"pvalue: ",pvalueS4)
SpaceResS450Dis.Draw()
ax =SpaceResS450Dis.GetXaxis()
#ax.SetRangeUser(SignalStartBin, SignalEndBin)
ax.SetTitle( " PosGap(mm) " )
ay = SpaceResS450Dis.GetYaxis()
ay.SetTitle( "Entries" )
ay.SetTitleSize(0.04)
ax.SetTitleOffset(1.0)
ay.SetTitleOffset(0.8)
ax.SetTitleSize(0.04)
ax.SetLabelSize(0.04)
ay.SetLabelSize(0.04)
ax.Draw("same")
ay.Draw("same")
c3.SaveAs("SpaceResS450Dis.png")

SpaceResS460Dis.SetLineColor(kBlue)
resultS4 = SpaceResS460Dis.Fit("gaus","S")
chi2S4 = resultS4.Chi2()
fitNDofS4 = resultS4.Ndf()
chiresultS4 =  chi2S4/fitNDofS4
pvalueS4 = resultS4.Prob()
print("ChiresultS4: ",chiresultS4,"pvalue: ",pvalueS4)
SpaceResS460Dis.Draw()
ax =SpaceResS460Dis.GetXaxis()
#ax.SetRangeUser(SignalStartBin, SignalEndBin)
ax.SetTitle( " PosGap(mm) " )
ay = SpaceResS460Dis.GetYaxis()
ay.SetTitle( "Entries" )
ay.SetTitleSize(0.04)
ax.SetTitleOffset(1.0)
ay.SetTitleOffset(0.8)
ax.SetTitleSize(0.04)
ax.SetLabelSize(0.04)
ay.SetLabelSize(0.04)
ax.Draw("same")
ay.Draw("same")
c3.SaveAs("SpaceResS460Dis.png")

SpaceResS470Dis.SetLineColor(kBlue)
resultS4 = SpaceResS470Dis.Fit("gaus","S")
chi2S4 = resultS4.Chi2()
fitNDofS4 = resultS4.Ndf()
chiresultS4 =  chi2S4/fitNDofS4
pvalueS4 = resultS4.Prob()
print("ChiresultS4: ",chiresultS4,"pvalue: ",pvalueS4)
SpaceResS470Dis.Draw()
ax =SpaceResS470Dis.GetXaxis()
#ax.SetRangeUser(SignalStartBin, SignalEndBin)
ax.SetTitle( " PosGap(mm) " )
ay = SpaceResS470Dis.GetYaxis()
ay.SetTitle( "Entries" )
ay.SetTitleSize(0.04)
ax.SetTitleOffset(1.0)
ay.SetTitleOffset(0.8)
ax.SetTitleSize(0.04)
ax.SetLabelSize(0.04)
ay.SetLabelSize(0.04)
ax.Draw("same")
ay.Draw("same")
c3.SaveAs("SpaceResS470Dis.png")

SpaceResS480Dis.SetLineColor(kBlue)
resultS4 = SpaceResS480Dis.Fit("gaus","S")
chi2S4 = resultS4.Chi2()
fitNDofS4 = resultS4.Ndf()
chiresultS4 =  chi2S4/fitNDofS4
pvalueS4 = resultS4.Prob()
print("ChiresultS4: ",chiresultS4,"pvalue: ",pvalueS4)
SpaceResS480Dis.Draw()
ax =SpaceResS480Dis.GetXaxis()
#ax.SetRangeUser(SignalStartBin, SignalEndBin)
ax.SetTitle( " PosGap(mm) " )
ay = SpaceResS480Dis.GetYaxis()
ay.SetTitle( "Entries" )
ay.SetTitleSize(0.04)
ax.SetTitleOffset(1.0)
ay.SetTitleOffset(0.8)
ax.SetTitleSize(0.04)
ax.SetLabelSize(0.04)
ay.SetLabelSize(0.04)
ax.Draw("same")
ay.Draw("same")
c3.SaveAs("SpaceResS480Dis.png")

SpaceResS490Dis.SetLineColor(kBlue)
resultS4 = SpaceResS490Dis.Fit("gaus","S")
chi2S4 = resultS4.Chi2()
fitNDofS4 = resultS4.Ndf()
chiresultS4 =  chi2S4/fitNDofS4
pvalueS4 = resultS4.Prob()
print("ChiresultS4: ",chiresultS4,"pvalue: ",pvalueS4)
SpaceResS490Dis.Draw()
ax =SpaceResS490Dis.GetXaxis()
#ax.SetRangeUser(SignalStartBin, SignalEndBin)
ax.SetTitle( " PosGap(mm) " )
ay = SpaceResS490Dis.GetYaxis()
ay.SetTitle( "Entries" )
ay.SetTitleSize(0.04)
ax.SetTitleOffset(1.0)
ay.SetTitleOffset(0.8)
ax.SetTitleSize(0.04)
ax.SetLabelSize(0.04)
ay.SetLabelSize(0.04)
ax.Draw("same")
ay.Draw("same")
c3.SaveAs("SpaceResS490Dis.png")

SpacialReS5Dis.SetLineColor(kBlue)
resultS5 = SpacialReS5Dis.Fit("gaus","S")
chi2S5 = resultS5.Chi2()
fitNDofS5 = resultS5.Ndf()
chiresultS5 =  chi2S5/fitNDofS5
pvalueS5 = resultS5.Prob()
print("ChiresultS5: ",chiresultS5,"pvalue: ",pvalueS5)
SpacialReS5Dis.Draw()
ax =SpacialReS5Dis.GetXaxis()
#ax.SetRangeUser(SignalStartBin, SignalEndBin)
ax.SetTitle( " PosGap(mm) " )
ay = SpacialReS5Dis.GetYaxis()
ay.SetTitle( "Entries" )
ay.SetTitleSize(0.04)
ax.SetTitleOffset(1.0)
ay.SetTitleOffset(0.8)
ax.SetTitleSize(0.04)
ax.SetLabelSize(0.04)
ay.SetLabelSize(0.04)
ax.Draw("same")
ay.Draw("same")
c3.SaveAs("SpacialReS5Dis.png")

SpaceResS550Dis.SetLineColor(kBlue)
resultS5 = SpaceResS550Dis.Fit("gaus","S")
chi2S5 = resultS5.Chi2()
fitNDofS5 = resultS5.Ndf()
chiresultS5 =  chi2S5/fitNDofS5
pvalueS5 = resultS5.Prob()
print("ChiresultS5: ",chiresultS5,"pvalue: ",pvalueS5)
SpaceResS550Dis.Draw()
ax =SpaceResS550Dis.GetXaxis()
#ax.SetRangeUser(SignalStartBin, SignalEndBin)
ax.SetTitle( " PosGap(mm) " )
ay = SpaceResS550Dis.GetYaxis()
ay.SetTitle( "Entries" )
ay.SetTitleSize(0.04)
ax.SetTitleOffset(1.0)
ay.SetTitleOffset(0.8)
ax.SetTitleSize(0.04)
ax.SetLabelSize(0.04)
ay.SetLabelSize(0.04)
ax.Draw("same")
ay.Draw("same")
c3.SaveAs("SpaceResS550Dis.png")

SpaceResS560Dis.SetLineColor(kBlue)
resultS5 = SpaceResS560Dis.Fit("gaus","S")
chi2S5 = resultS5.Chi2()
fitNDofS5 = resultS5.Ndf()
chiresultS5 =  chi2S5/fitNDofS5
pvalueS5 = resultS5.Prob()
print("ChiresultS5: ",chiresultS5,"pvalue: ",pvalueS5)
SpaceResS560Dis.Draw()
ax =SpaceResS560Dis.GetXaxis()
#ax.SetRangeUser(SignalStartBin, SignalEndBin)
ax.SetTitle( " PosGap(mm) " )
ay = SpaceResS560Dis.GetYaxis()
ay.SetTitle( "Entries" )
ay.SetTitleSize(0.04)
ax.SetTitleOffset(1.0)
ay.SetTitleOffset(0.8)
ax.SetTitleSize(0.04)
ax.SetLabelSize(0.04)
ay.SetLabelSize(0.04)
ax.Draw("same")
ay.Draw("same")
c3.SaveAs("SpaceResS560Dis.png")

SpaceResS570Dis.SetLineColor(kBlue)
resultS5 = SpaceResS570Dis.Fit("gaus","S")
chi2S5 = resultS5.Chi2()
fitNDofS5 = resultS5.Ndf()
chiresultS5 =  chi2S5/fitNDofS5
pvalueS5 = resultS5.Prob()
print("ChiresultS5: ",chiresultS5,"pvalue: ",pvalueS5)
SpaceResS570Dis.Draw()
ax =SpaceResS570Dis.GetXaxis()
#ax.SetRangeUser(SignalStartBin, SignalEndBin)
ax.SetTitle( " PosGap(mm) " )
ay = SpaceResS570Dis.GetYaxis()
ay.SetTitle( "Entries" )
ay.SetTitleSize(0.04)
ax.SetTitleOffset(1.0)
ay.SetTitleOffset(0.8)
ax.SetTitleSize(0.04)
ax.SetLabelSize(0.04)
ay.SetLabelSize(0.04)
ax.Draw("same")
ay.Draw("same")
c3.SaveAs("SpaceResS570Dis.png")

SpaceResS580Dis.SetLineColor(kBlue)
resultS5 = SpaceResS580Dis.Fit("gaus","S")
chi2S5 = resultS5.Chi2()
fitNDofS5 = resultS5.Ndf()
chiresultS5 =  chi2S5/fitNDofS5
pvalueS5 = resultS5.Prob()
print("ChiresultS5: ",chiresultS5,"pvalue: ",pvalueS5)
SpaceResS580Dis.Draw()
ax =SpaceResS580Dis.GetXaxis()
#ax.SetRangeUser(SignalStartBin, SignalEndBin)
ax.SetTitle( " PosGap(mm) " )
ay = SpaceResS580Dis.GetYaxis()
ay.SetTitle( "Entries" )
ay.SetTitleSize(0.04)
ax.SetTitleOffset(1.0)
ay.SetTitleOffset(0.8)
ax.SetTitleSize(0.04)
ax.SetLabelSize(0.04)
ay.SetLabelSize(0.04)
ax.Draw("same")
ay.Draw("same")
c3.SaveAs("SpaceResS580Dis.png")

SpaceResS590Dis.SetLineColor(kBlue)
resultS5 = SpaceResS590Dis.Fit("gaus","S")
chi2S5 = resultS5.Chi2()
fitNDofS5 = resultS5.Ndf()
chiresultS5 =  chi2S5/fitNDofS5
pvalueS5 = resultS5.Prob()
print("ChiresultS5: ",chiresultS5,"pvalue: ",pvalueS5)
SpaceResS590Dis.Draw()
ax =SpaceResS590Dis.GetXaxis()
#ax.SetRangeUser(SignalStartBin, SignalEndBin)
ax.SetTitle( " PosGap(mm) " )
ay = SpaceResS590Dis.GetYaxis()
ay.SetTitle( "Entries" )
ay.SetTitleSize(0.04)
ax.SetTitleOffset(1.0)
ay.SetTitleOffset(0.8)
ax.SetTitleSize(0.04)
ax.SetLabelSize(0.04)
ay.SetLabelSize(0.04)
ax.Draw("same")
ay.Draw("same")
c3.SaveAs("SpaceResS590Dis.png")

