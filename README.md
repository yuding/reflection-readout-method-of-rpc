# Reflection Readout Method of RPC

# Data
Include the reflected signals which are recorded by oscilloscope and saved as .trc file and reference signals which are recorded by SADC and saved as .dat file
Reference readout panel has 16 channels which are all used.
6 middle channels of reflection readout panel are used.

# ParaAndDataInput.py
Parameter setting and open the data file to read signal information.

# Channel.py 
Analyze every channel of reference readout panel and reflection readout panel to check if this channel has signals

# Event.py
Analyze every event to find the signals on reference readout panel and reflection readout panel.

# DrawWaveform.py
Draw waveform on every channel

# Analysis.py
Analyze all events to get the relationship between timegap and hit channel on reference panel so that we can calculate spatial resolution then.

#spatialReCal.py
Calculate the spatial resolution eventually.
